angular.module('RdmApp').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/dashboard");  
	////console.log($urlRouterProvider);
    $stateProvider
    .state('dashboard', {
            url: "/dashboard",
            templateUrl: "guru/pages/default"+versionApp,            
            data: {pageTitle: 'Beranda RDM',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "DashboardCtrl",
            sidebar:false,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) { 
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/DashboardCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        }) 

	.state('datakelas', {
            url: "/datakelas",
            templateUrl: "guru/pages/datakelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "datakelasCtrl",
            sidebar:false,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/datakelasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
	.state('ekstra', {
            url: "/ekstra",
            templateUrl: "guru/pages/ekstra"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "ekstraCtrl",
            sidebar:false,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/ekstraCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
	.state('walas', {
            url: "/walas", 
            templateUrl: "guru/pages/walas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "walasCtrl",
            sidebar:false,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/walasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('walas.siswa', {
        url: "/siswa",
        templateUrl: "guru/pages/siswa/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "siswaCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/siswaCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('walas.rapor', {
        url: "/rapor",
        templateUrl: "guru/pages/rapor/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "raporCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/raporCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('walas.absen', {
        url: "/absen",
        templateUrl: "guru/pages/absen/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "absenCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/absenCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('walas.catatan', {
        url: "/catatan",
        templateUrl: "guru/pages/catatan/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "catatanCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/catatanCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('walas.nilai', {
        url: "/nilai",
        templateUrl: "guru/pages/nilai/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "nilaiCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/nilaiCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('walas.prestasi', {
        url: "/prestasi",
        templateUrl: "guru/pages/prestasi/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "prestasiCtrl",
        sidebar:false,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/prestasiCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
    .state('walas.sikapsos', {
        url: "/sikapsos",
        templateUrl: "guru/pages/sikapsos/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "sikapsosCtrl",
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/sikapsosCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
    .state('walas.sikapspiritual', {
        url: "/sikapspiritual",
        templateUrl: "guru/pages/sikapspiritual/walas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "sikapspiritualCtrl",
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/sikapspiritualCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })

    .state('kelas', {
            url: "/kelas/:idkelas",
            templateUrl: "guru/pages/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "kelasCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/kelasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.beranda', {
            url: "/beranda",
            templateUrl: "guru/pages/beranda/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "berandaCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/berandaCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.bobot', {
            url: "/bobot",
            templateUrl: "guru/pages/bobot/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "bobotCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/bobotCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.pengetahuan', {
            url: "/pengetahuan",
            templateUrl: "guru/pages/pengetahuan/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "pengetahuanCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/pengetahuanCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.harian', {
            url: "/harian",
            templateUrl: "guru/pages/harian/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "harianCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/harianCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.paspat', {
            url: "/paspat",
            templateUrl: "guru/pages/paspat/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "paspatCtrl",
            sidebar:true,
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/guru/paspatCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
    })
	.state('kelas.keterampilan', {
        url: "/keterampilan",
        templateUrl: "guru/pages/keterampilan/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "keterampilanCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/keterampilanCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('kelas.unjukkerja', {
        url: "/unjukerja",
        templateUrl: "guru/pages/unjukkerja/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "unjukkerjaCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/unjukkerjaCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('kelas.nilaiketerampilan', {
        url: "/nilaiketerampilan",
        templateUrl: "guru/pages/nilaiketerampilan/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "nilaiketerampilanCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/nilaiketerampilanCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('kelas.proyek', {
        url: "/proyek",
        templateUrl: "guru/pages/proyek/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "proyekCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/proyekCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('kelas.portofolio', {
        url: "/portofolio",
        templateUrl: "guru/pages/portofolio/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "portofolioCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/portofolioCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })
	.state('kelas.sikap', {
        url: "/sikap",
        templateUrl: "guru/pages/sikap/kelas"+versionApp,            
        data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
        controller: "sikapCtrl",
        sidebar:true,
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'RdmApp',
                    insertBefore: '#ng_load_plugins_before', 
                    files: [
                        'assets/angular/js/controller/guru/sikapCtrl.js'+versionApp
                    ] 
                });
            }]
        }
    })

}]);