angular.module('RdmApp').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/dashboard");  
	////console.log($urlRouterProvider);
    $stateProvider
    .state('dashboard', {
            url: "/dashboard",
            templateUrl: "proktor/pages/default"+versionApp,            
            data: {pageTitle: 'Beranda RDM',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "DashboardCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) { 
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/DashboardCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
        .state('pengaturan', {
            url: "/pengaturan",
            templateUrl: "proktor/pages/pengaturan"+versionApp,
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
            controller: "pengaturanCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'assets/angular/js/controller/proktor/pengaturanCtrl.js'+versionApp
                        ]
                    });
                }]
            }
        })
		.state('profile', {
            url: "/profile",
            templateUrl: "proktor/pages/profile"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "profileCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/profileCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('semester', {
            url: "/semester",
            templateUrl: "proktor/pages/semester"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "semesterCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/semesterCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('aturcetak', {
            url: "/aturcetak",
            templateUrl: "proktor/pages/aturcetak"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "aturcetakCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/aturcetakCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('mapel', {
            url: "/mapel",
            templateUrl: "proktor/pages/mapel"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "mapelCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/mapelCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('kelas', {
            url: "/kelas",
            templateUrl: "proktor/pages/kelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "kelasCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/kelasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('dataguru', {
            url: "/dataguru",
            templateUrl: "proktor/pages/guru"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "guruCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/guruCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })

		.state('datasiswa', {
            url: "/datasiswa",
            templateUrl: "proktor/pages/siswa"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "siswaCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/siswaCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('dataajar', {
            url: "/dataajar",
            templateUrl: "proktor/pages/ajar"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "ajarCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/ajarCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('naikkelas', {
            url: "/naikkelas",
            templateUrl: "proktor/pages/naikkelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "naikkelasCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/naikkelasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })
		.state('pindahkelas', {
            url: "/pindahkelas",
            templateUrl: "proktor/pages/pindahkelas"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "pindahkelasCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/pindahkelasCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })


		.state('ekstrakurikuler', {
            url: "/ekstrakurikuler",
            templateUrl: "proktor/pages/ekstrakurikuler"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "ekstraCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/ekstraCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })

		.state('kkmtingkat', {
            url: "/kkmtingkat",
            templateUrl: "proktor/pages/kkmtingkat"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "kkmtingkatCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/kkmtingkatCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })

		.state('backuprestore', {
            url: "/backuprestore",
            templateUrl: "proktor/pages/backuprestore"+versionApp,            
            data: {pageTitle: 'Kelas Siswa',pageThumb: 'Home',pageSubThumb:{0:'Login'}},
			controller: "backuprestoreCtrl",
			resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RdmApp',
                        insertBefore: '#ng_load_plugins_before', 
                        files: [
                            'assets/angular/js/controller/proktor/backuprestoreCtrl.js'+versionApp
                        ] 
                    });
                }]
            }
        })


}]);