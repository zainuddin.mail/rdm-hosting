angular.module('RdmApp').controller('siswaCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,FileUploader) {
	//$rootScope.tingkat = [];
	$scope.dtInstance = {};
	$scope.dataEdit = {}; 
	$scope.savedataEdit = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("proktor/siswa/save",$scope.dataEdit,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.reloadTable(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	$scope.dodataDelete = function(id) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus data ini?",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("proktor/siswa/delete",{siswa_id:id},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){
							$scope.reloadTable(); 
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})

	}
	$scope.reloadTable = function() {
		$rootScope.datasiswa = {};
		if($scope.selectData.kelas_id !==""){
			$rootScope.getdatasiswa($scope.selectData);
		}
	}
	$scope.dodataTambah = function() {
		if($scope.selectData.kelas_id == ""){
			Swal.fire({
				type: 'warning',
				title: 'Perhatian',
				showConfirmButton: false,
				 html: "Pilih kelas terlebih dahulu",
				 timer: 1000,
			})
		}else{
			$scope.dataEdit = {};
			$scope.dataEdit.kelas_id = $scope.selectData.kelas_id;
			$scope.dataEdit.tingkat_id = $scope.selectData.tingkat_id;
			$scope.dataEdit.edit = false;
			$(".edit-modal").modal("show");
			$('.modal-dialog').draggable({
				handle: ".modal-header"
			  });
	
		}

	}
	$scope.dodataUpload = function() {
		if($scope.selectData.kelas_id == ""){
			Swal.fire({
				type: 'warning',
				title: 'Perhatian',
				showConfirmButton: false,
				 html: "Pilih kelas terlebih dahulu",
				 timer: 1000,
			})
		}else{
			$scope.dataEdit = {};
			$scope.dataEdit.kelas_id = $scope.selectData.kelas_id;
			$scope.dataEdit.tingkat_id = $scope.selectData.tingkat_id;
			$scope.dataEdit.edit = false;
			$(".upload-modal").modal("show");
		}

	}
	$scope.clearKelas = function(jsonEdit) {
		$scope.selectData.kelas_id = "";
		$rootScope.datasiswa ={};
	}
	$scope.dodataEdit = function(jsonEdit) {
		$scope.dataEdit =  jsonEdit;		
		//console.log($scope.dataEdit);
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
	}
	$scope.selectData = {};
	$scope.selectData.kelas_id = "";
	$scope.selectData.tingkat_id = "";
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,2,3,4,5,6]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Data Siswa','RDM')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						columns: [ 0,2,3,4,5,6]
					  },
					  titleAttr: 'Excel'
				  }
			  ])
			.withOption('paging', true)	
			.withOption('stateSave', true)
			.withOption('processing', true)
			.withOption('drawCallback', function() {
				$('.lazy').Lazy();
				try{
					$scope.$apply();
					}catch(ex){
						
					}
			})
			.withPaginationType('full_numbers'); 
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'proktor/siswa/import' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$scope.reloadTable();
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'proktor/siswa/import?kelas_id='+$scope.selectData.kelas_id+'&tingkat_id='+$scope.selectData.tingkat_id  ;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};
	
}); 