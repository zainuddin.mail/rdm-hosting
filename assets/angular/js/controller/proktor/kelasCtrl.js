angular.module('RdmApp').controller('kelasCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.tingkat = [];

	$scope.dtInstance = {};
	$scope.datakelasedit = {}; 
	$scope.selectData = {}; 
	$scope.selectData.tingkat_id = ''; 
	$scope.deletekelas = function(kelas) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, Hapus!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus kelas ini?",
		}).then((result) => {
			//console.log(result);
			if (result.value) { 
				$.blockUI({ });
				ApiServer.post("proktor/kelas/delkelas",kelas,function(response) {
						$.unblockUI({ });
						if(response.success){
							$rootScope.getdatakelas(); 
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								html: 'Data Berhasil Dihapus',
								timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								html: response.message
							})
						}				
				});
			}
		})

	}
	$scope.saveeditkelas = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("proktor/kelas/save",$scope.datakelasedit,function(response) {
			////console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getdatakelas(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	$scope.tambahKelas = function(data) {
		$scope.datakelasedit= {}; 
		$scope.datakelasedit.edit = false; 
	}
	$scope.editkelas = function(data) {
		$scope.datakelasedit= data; 
		$scope.datakelasedit.edit = true; 
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
	}
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,1,2,3,]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						  columns: [ 0,2,3,]
						},
					  titleAttr: 'Excel'
				  }
			  ])
			  .withOption('stateSave', true)
			  .withOption('paging', true)	
			.withOption('processing', true)
			.withPaginationType('full_numbers'); 

}); 