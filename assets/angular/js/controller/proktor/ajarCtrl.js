angular.module('RdmApp').controller('ajarCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,FileUploader) {
	//$rootScope.tingkat = [];
	$scope.dtInstance = {};
	$scope.dataEdit = {}; 
	$scope.setmengjar = function(data,datarow) {
		$.blockUI({ });
		ApiServer.post("proktor/ajar/save",data,function(response) {
			//console.log(response);
				$.unblockUI({ });
				//$scope.reloadTable(); 
				if(response.success){
					datarow.dataEdit.delete = response.data;
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					$scope.reloadTable();
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.dodataDelete = function(id,data) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus data ini?",
		}).then((result) => {
			console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("proktor/ajar/delete",{ajar_id:id.delete},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){
							data.select = {};
							data.select.guru_id = "";
							id.delete = "";
							id.edit = "";
							//$scope.reloadTable();
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							$scope.reloadTable();
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})

	}
	$rootScope.dataajar = {};
	
	$scope.reloadTable = function() {
		$rootScope.dataajar = {};
		if($scope.selectData.kelas_id !==""){
			$rootScope.getdataajar($scope.selectData);
		}
	}

	$scope.dodataTambah = function() {
		if($scope.selectData.kelas_id == ""){
			Swal.fire({
				type: 'warning',
				title: 'Perhatian',
				showConfirmButton: false,
				 html: "Pilih kelas terlebih dahulu",
				 timer: 1000,
			})
		}else{
			$scope.dataEdit = {};
			$scope.dataEdit.kelas_id = $scope.selectData.kelas_id;
			$scope.dataEdit.tingkat_id = $scope.selectData.tingkat_id;
			$scope.dataEdit.edit = false;
			$(".edit-modal").modal("show");
			$('.modal-dialog').draggable({
				handle: ".modal-header"
			  });
	
		}

	}
	$scope.changeTingkat = function() {
		$rootScope.dataajar = {};
		$scope.selectData.kelas_id = "";
	}
	$scope.dodataUpload = function() {
		if($scope.selectData.kelas_id == ""){
			Swal.fire({
				type: 'warning',
				title: 'Perhatian',
				showConfirmButton: false,
				 html: "Pilih kelas terlebih dahulu",
				 timer: 1000,
			})
		}else{
			$scope.dataEdit = {};
			$scope.dataEdit.kelas_id = $scope.selectData.kelas_id;
			$scope.dataEdit.tingkat_id = $scope.selectData.tingkat_id;
			$scope.dataEdit.edit = false;
			$(".upload-modal").modal("show");

	
		}

	}
	$scope.dodataEdit = function(jsonEdit) {
		$scope.dataEdit =  jsonEdit;		
		//console.log($scope.dataEdit);
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
	$scope.selectData = {};
	$scope.selectData.kelas_id = "";
	$scope.selectData.tingkat_id = "";
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,1,2,3,]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Absen ajar','Rapor Digital Madrasah')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						  columns: [ 0,2,3,]
						},
					  titleAttr: 'Excel'
				  }
			  ])
			.withOption('paging', true)	
			.withOption('stateSave', true)
			.withOption('processing', true)
			.withOption('responsive', true)
			.withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]])
			.withOption('columnDefs',[
				{ "width": "2%", "targets": 0 },
				{ "width": "20%", "targets": -2 },
				{ "width": "20%", "targets": 1 },
				{ responsivePriority: 1, targets: 2 },
				{ responsivePriority: 2, targets: -1 }
			])
			.withOption('drawCallback', function() {
				$('.lazy').Lazy();
				try{
					$scope.$apply();
					}catch(ex){
						
					}
			});
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'proktor/ajar/import' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$scope.reloadTable();
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'proktor/ajar/import?kelas_id='+$scope.selectData.kelas_id+'&tingkat_id='+$scope.selectData.tingkat_id  ;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};
	
}); 