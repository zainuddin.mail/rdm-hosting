angular.module('RdmApp').controller('aturcetakCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];
   $scope.saveTempatCetak = function() {
		$.blockUI({ });
        //console.log($rootScope.aturcetak);
		ApiServer.post("proktor/semester/cetaksave",{cetak_tanggal:$rootScope.aturcetak.cetak_tanggal,cetak_tempat:$rootScope.aturcetak.cetak_tempat,cetak_watermark:$rootScope.aturcetak.cetak_watermark},function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
					$rootScope.getsemester();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
			}else if(response==null){
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: 'Data Gagal Disimpan'
					})
			}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
			}			
		});
	}
	
}); 