angular.module('RdmApp').controller('mapelCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	$rootScope.dataEdit = {};
    $scope.syncronMapel = function() {
		$.blockUI({ });
		ApiServer.get("proktor/mapel/syncron",function(response) {
			$.unblockUI({ });
			if(response.success){			
				$rootScope.getdataMapel();
				Swal.fire({
					type: 'success',
					title: 'Berhasil',
					showConfirmButton: false,
					 html: 'Data Berhasil Disimpan',
					 timer: 1500,
				})
			}else{ 
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
					 html: response.message
				})
			}
		});
	}
	$scope.savedataEdit = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("proktor/mapel/save",$rootScope.dataEdit,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){			
					$rootScope.getdataMapel();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});	
	}
	$scope.dodatamoveUp = function(id) { 
		console.log(id);
		$.blockUI({ });
		ApiServer.post("proktor/mapel/moveup",{mapel_id:id},function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getdataMapel();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
							html: 'Nomor Urut Mapel berhasil di naikkan',
							timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						html: response.message
					})
				}				
		});
	}
	$scope.dodatamoveDown = function(id) { 
		console.log(id);
		$.blockUI({ });
		ApiServer.post("proktor/mapel/movedown",{mapel_id:id},function(response) {
				$.unblockUI({ });
				if(response.success){
					$rootScope.getdataMapel();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
							html: 'Nomor Urut Mapel berhasil di turunkan',
							timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						html: response.message
					})
				}				
		});
	}
	$scope.dodataDelete = function(id) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus data ini?",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("proktor/mapel/delete",{mapel_id:id},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){
							$rootScope.getdataMapel();
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})

	}

	$scope.tambahMapel = function() {
		$rootScope.dataEdit =  {};		
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
	}
	$scope.dodataEdit = function(jsonEdit) {
		$rootScope.dataEdit =  jsonEdit;		
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		});
	}
	$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('searching', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,1,2,3,]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						  columns: [ 0,2,3,]
						},
					  titleAttr: 'Excel'
				  }
			  ])
			  .withOption('stateSave', true)
			  .withOption('paging', true)
			.withPaginationType('full_numbers'); 
		
}); 