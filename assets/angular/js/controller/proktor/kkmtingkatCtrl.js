angular.module('RdmApp').controller('kkmtingkatCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.tingkat = [];

	$scope.dtInstance = {};

	$scope.kkmChange = function(data,nilai) {
		//console.log(data);
		var pembagi = Math.round((100-parseInt(nilai))/3,0);
		var max_b = 100- pembagi;
		var min_b = parseInt(nilai) + pembagi;
		if(parseInt(nilai) < 98){
			angular.forEach(data, function(value, key) {
				if(value.kkmgrade_kode =='A'){
					value.kkmgrade_max = 100;
					value.kkmgrade_min = max_b + 1;
				}else if(value.kkmgrade_kode =='B'){
					value.kkmgrade_max = max_b;
					value.kkmgrade_min = min_b;
				}else if(value.kkmgrade_kode =='C'){
					value.kkmgrade_max = min_b-1;
					value.kkmgrade_min = nilai;
				}else{
					value.kkmgrade_max = parseInt(nilai) -1 ;
					value.kkmgrade_min = 0;
				}
			});
		}else{
			Swal.fire({
				type: 'warning',
				title: 'Perhatian',
				showConfirmButton: true,
				 html: 'Nilai KKM tidak boleh melebihi angka 97',
			})			
		}

	}
	$scope.saveKkmTingkat = function(data) {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("proktor/kkmtingkat/save",data,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){			
					$rootScope.getdatakkm();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						html: response.message
					}).then((result) => {
						if (result.value) {  
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	$scope.editekstra = function(ekstra_id,ekstra_nama,tingkat_id,jurusan_id,guru_id,edit) {
		$scope.dataekstraedit.ekstra_id = ekstra_id; 
		$scope.dataekstraedit.ekstra_nama = ekstra_nama; 
		$scope.dataekstraedit.tingkat_id = tingkat_id; 
		$scope.dataekstraedit.jurusan_id = jurusan_id; 
		$scope.dataekstraedit.guru_id = guru_id; 
		$scope.dataekstraedit.edit = edit; 
			try{
			$scope.$apply();
			}catch(ex){
				
			}
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
	$scope.currPg = 0;
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					//console.log(pgNo);
					$scope.currPg = pgNo.start;
				}
			})
			.withOption('stateSave', true)
			.withOption('paging', true)	
			.withOption('processing', true)
			.withPaginationType('full_numbers'); 


    $rootScope.datakkmtingkat = [];
	$rootScope.getdatakkm = function() {
		$(".table-responsive").block({ });
		ApiServer.get("proktor/kkmtingkat/data",function(response) {
            $(".table-responsive").unblock({ });
			$rootScope.datakkmtingkat = response.data; 
		});
	}
	//console.log($rootScope.datakkmtingkat.length);
	if($rootScope.datakkmtingkat.length == 0){
		$rootScope.getdatakkm();
	}
}); 