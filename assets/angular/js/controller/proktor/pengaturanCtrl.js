angular.module('RdmApp').controller('pengaturanCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];

    $scope.savetahunajaran = function() {
		$.blockUI({ });
		ApiServer.post("proktor/pengaturan/save",{tahunajaran_id:$rootScope.tahunajaran.aktif,semester_id:$rootScope.datasemester.aktif,sistem_penilaian:$rootScope.penilaian.system},function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
				$rootScope.getpengaturan();
				Swal.fire({
					type: 'success',
					title: 'Berhasil',
					showConfirmButton: false,
						html: 'Data Berhasil Disimpan',
						timer: 1000,
				})					
			}else if(response==null){
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
						html: 'Data Gagal Disimpan'
				})
			}else{
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
						html: response.message
				})
			}			
		});
	}
		
}); 