angular.module('RdmApp').controller('ekstraCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.tingkat = [];

	$scope.dtInstance = {};
	$scope.newextra = {}; 
	$scope.newextra.ekstrakurikuler_nama = ""; 
	$scope.newextra.guru_id = ""; 
	$scope.setpembina = function(data) {
		//if($rootScope.rendering){
			$scope.newextra =  data;
			$scope.saveeditekstra1();		
		//}else{
		//	console.log("Blom rendering");
		//}
	}
	$scope.saveeditekstra1 = function() {
		$.blockUI({ });
		ApiServer.post("proktor/ekstrakurikuler/save",$scope.newextra,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.newextra = {}; 
					$scope.newextra.ekstrakurikuler_nama = ""; 
					$scope.newextra.guru_id = ""; 				
					$rootScope.getdataekstra(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					$rootScope.getdataekstra(); 
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.saveeditekstra = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("proktor/ekstrakurikuler/save",$scope.newextra,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.newextra = {}; 
					$scope.newextra.ekstrakurikuler_nama = ""; 
					$scope.newextra.guru_id = ""; 				
					$rootScope.getdataekstra(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	$scope.dodataDelete = function(ekstrakurikuler_id) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus data ini?",
		}).then((result) => {
			if (result.value) {
				$.blockUI({ });
				ApiServer.post("proktor/ekstrakurikuler/delete",{"ekstrakurikuler_id":ekstrakurikuler_id},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){				
							$rootScope.getdataekstra(); 
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								html: 'Data Berhasil Disimpan',
								timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								html: response.message
							})
						}				
				});
			}
		})
	}
	$scope.dodataEdit =  function(data){
		$scope.editekstra(data.ekstrakurikuler_id,data.ekstrakurikuler_nama,data.guru_id,true);
	}
	$scope.editekstra = function(ekstra_id,ekstra_nama,guru_id,edit) {
		$scope.newextra.ekstrakurikuler_id = ekstra_id; 
		$scope.newextra.ekstrakurikuler_nama = ekstra_nama; 
		$scope.newextra.guru_id = guru_id; 
		$scope.newextra.edit = edit; 
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
	$scope.currPg = 0;
	$rootScope.getdataekstra = function() {
		//$rootScope.rendering =false;
		ApiServer.get("proktor/ekstrakurikuler/data",function(response) {
			$.unblockUI({});
			$rootScope.dataekstrakurikuler1 = response.data; 
			$rootScope.datapembina1 = response.guru; 
		});
	}

	//setTimeout(function(){
		$rootScope.getdataekstra();
	//},1000);

	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,1,2,3,]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						  columns: [ 0,2,3,]
						},
					  titleAttr: 'Excel'
				  }
			  ])
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					//console.log(pgNo);
					$scope.currPg = pgNo.start;
				}
			})
			.withOption('paging', true)	
			.withOption('processing', false)
			.withPaginationType('full_numbers'); 

}); 