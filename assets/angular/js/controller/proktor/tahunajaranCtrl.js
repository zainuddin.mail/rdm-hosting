angular.module('RdmApp').controller('tahunajaranCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];
    $scope.savetahunajaran = function() {
		$.blockUI({ });
		ApiServer.post("proktor/tahunajaran/save",{aktif:$rootScope.tahunajaran.aktif},function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
				$rootScope.gettahunajaran();
				Swal.fire({
					type: 'success',
					title: 'Berhasil',
					showConfirmButton: false,
						html: 'Data Berhasil Disimpan',
						timer: 1000,
				})					
			}else if(response==null){
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
						html: 'Data Gagal Disimpan'
				})
			}else{
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
						html: response.message
				})
			}			
		});
	}
		
}); 