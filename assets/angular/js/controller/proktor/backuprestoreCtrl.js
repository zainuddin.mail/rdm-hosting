angular.module('RdmApp').controller('backuprestoreCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,FileUploader) {
	//$rootScope.tingkat = [];

	$scope.dtInstance = {};
	$scope.newextra = {}; 
	$scope.newextra.ekstrakurikuler_nama = ""; 
	$scope.newextra.guru_id = ""; 
	$rootScope.getdatabck = function() {
		$.blockUI({});
		ApiServer.get("proktor/backuprestore/data",function(response) {
			$.unblockUI({});
			$rootScope.databackup = response.data;
		});
	}
	$rootScope.doBackupData = function() {
		$.blockUI({});
        $.post("proktor/backuprestore/backup") 
            .done(function (response,status, xhr) {
                $.unblockUI({});
                $rootScope.getdatabck(); 
				var ct = xhr.getResponseHeader("content-type") || "";
				if (ct == "application/json") {
                    if(response.success){
                        Swal.fire({
                            type: 'success',
                            title: 'Berhasil',
                            text: 'Backup File Berhasil Dibuat',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }else{
                        Swal.fire({
                            type: 'error',
                            title: 'Gagal Membuat Backup',
                            text: response.message,
                            showConfirmButton: true
                        })
                    }
				}else{
					Swal.fire({
                        type: 'success',
                        title: 'Berhasil',
                        text: 'Backup File Berhasil Dibuat',
                        showConfirmButton: false,
                        timer: 1500
                    })
				}
			})
            .fail(function(data, status, headers, config) {	
                $.unblockUI({});			
                $rootScope.getdatabck(); 
              	Swal.fire({
					type: 'error',
					title: 'Gagal menyimpan',
					text: 'Koneksi keserver gagal',
					showConfirmButton: true
				})
				callback({success:false,message:'Gagal menghubungi server.'});
            });
	}
    $rootScope.getdatabck();
	$scope.dodataDelete = function(filename) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus backup ini?",
		}).then((result) => {
			if (result.value) {
				$.blockUI({ });
				ApiServer.post("proktor/backuprestore/delete",{"filename":filename},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){				
                            $rootScope.getdatabck(); 
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								html: 'Data Berhasil Disimpan',
								timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								html: response.message
							})
						}				
				});
			}
		})
	}
	$scope.dodataEdit =  function(data){
		$scope.editekstra(data.ekstrakurikuler_id,data.ekstrakurikuler_nama,data.guru_id,true);
	}
	$scope.restore = function() {
		$(".upload-modal").modal("show");
	}
	$scope.currPg = 0;
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					//console.log(pgNo);
					$scope.currPg = pgNo.start;
				}
			})
			.withOption('stateSave', true)
			.withOption('paging', true)	
			.withOption('processing', true)
			.withPaginationType('full_numbers'); 


    var uploader = $scope.uploader = new FileUploader({
        url: base_url + 'proktor/siswa/import' 
    });
            
  $scope.processRestore = function(index,start){
    var percent = Math.round(100*(index/$scope.datarestore.length));
    $.blockUI({ 
        message: '<h2>Harap bersabar!!</h2><h4>'+ percent +'% data sudah direstore!</h4>',
        baseZ: 9999
    });
    if(index < $scope.datarestore.length){
        $scope.restoredata = {};
        $scope.restoredata = $scope.datarestore[index];
        $scope.restoredata.start = start;
        ApiServer.post("proktor/backuprestore/prosesfile",$scope.datarestore[index],function(response) {
            if(response.success){
                if(response.pending == 0){
                    $scope.processRestore(index+1,0);
                }else{
                    $scope.processRestore(index,response.pending);
                }
            }else{
                $rootScope.getdatabck();
                $.unblockUI({});
                Swal.fire({
                    type: 'error',
                    title: 'Gagal',
                    showConfirmButton: true,
                    html: 'Data Gagal Direstore'
                })
            }
            
        });
      }else{
        $rootScope.getdatabck();   
        $.unblockUI({});
        Swal.fire({
            type: 'success',
            title: 'Berhasil',
            showConfirmButton: true,
            html: 'Data Berhasil Direstore'
        }).then((result) => {
			if (result.value) {
                $rootScope.reloaddataProktor();
            }
        })
      }
  }  
  
            $scope.berhasilaa = 0;
            $scope.gandaaa = 0;
            $scope.gagalaa = 0;
        
            // FILTERS
            $scope.hapussemua = function() {
                uploader.clearQueue();
                $scope.berhasilaa = 0;
                $scope.gandaaa = 0;
                $scope.gagalaa = 0;
        
            }
        
            uploader.filters.push({
                name: 'customFilter',
                fn: function(item /*{File|FileLikeObject}*/, options) {
                    return this.queue.length < 100;
                }
            });
            uploader.filters.push({
                name: 'imageFilter',
                fn: function(item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
                    console.info('type', type);
                    if('|rdm|'.indexOf(type) !== -1){
                        return true;
                    }else{
                        $(".upload-modal").modal("hide");
                        Swal.fire({
                            type: 'error',
                            title: 'File backup',
                            showConfirmButton: true,
                            html: 'File yang anda upload bukan file Backup RDM, pastikan file backup memiliki extensi .rdm'
                        }).then((result) => {
                            //console.log(result);
                            if (result.value) {  
                                uploader.clearQueue();
                                $(".upload-modal").modal("show");
                            }
                        })
                    }
                }
            });
        
            // CALLBACKS
            uploader.onSuccessItem = function(fileItem, response, status, headers) {
                //console.log(response);
                $scope.$apply();
                if(response.success){
                    $scope.datarestore = response.data;
                    $(".upload-modal").modal("hide");
                    if($scope.datarestore !== null && $scope.datarestore.length > 0){
                        $scope.processRestore(0,0);
                    }else{
                        Swal.fire({
                            type: 'error',
                            title: 'Gagal',
                            showConfirmButton: true,
                            html: response.message
                        })
                    }
                }else{
                    Swal.fire({
                        type: 'error',
                        title: 'Gagal',
                        showConfirmButton: true,
                        html: response.message
                    })
                }
                console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
            };
            uploader.onCompleteAll = function() {
                uploader.clearQueue();
             };
            uploader.onBeforeUploadItem = function(item) {
                item.url = base_url + 'proktor/backuprestore/restore';
            };
            uploader.onAfterAddingAll = function(addedFileItems) {
                uploader.uploadAll();
            };
        

}); 