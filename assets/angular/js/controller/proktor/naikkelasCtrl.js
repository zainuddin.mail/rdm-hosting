angular.module('RdmApp').controller('naikkelasCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.tingkat = [];
	$scope.dtInstance = {};
	$scope.dtInstance2 = {};
	$scope.dataEdit = {}; 
	$scope.datakelasedit = {}; 
	$scope.prosesNaik = function() {
		$scope.filteredArray = $scope.datasiswaasal.filter(function (siswa) {
			return (siswa.select == true);
		});
		var dataNaik = {};
		dataNaik.kelas_id = $scope.tujuanData.kelas_id;
		dataNaik.tahunajaran_id = $scope.tujuanData.tahunajaran_id;
		dataNaik.tingkat_id = $scope.tujuanData.tingkat_id;
		dataNaik.data = $scope.filteredArray;
		//console.log(dataNaik);
		$.blockUI({ });
		ApiServer.post("proktor/naikkelas/save",dataNaik,function(response) {
				$.unblockUI({ });
				if(response.success){
					$scope.reloadTable(); 
					$scope.reloadTableTujuan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.dismiss === Swal.DismissReason.timer) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	$scope.batalNaik = function() {
		$scope.filteredArray = $scope.datasiswatujuan.filter(function (siswa) {
			return (siswa.select == true);
		});
		var dataNaik = {};
		dataNaik.kelas_id = $scope.selectData.kelas_id;
		dataNaik.tahunajaran_id = $scope.selectData.tahunajaran_id;
		dataNaik.tingkat_id = $scope.selectData.tingkat_id;
		dataNaik.data = $scope.filteredArray;
		//console.log(dataNaik);
		$.blockUI({ });
		ApiServer.post("proktor/naikkelas/batalnaik",dataNaik,function(response) {
				$.unblockUI({ });
				if(response.success){
					$scope.reloadTable(); 
					$scope.reloadTableTujuan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });					
						}
					})
				}				
		});
	}
	$rootScope.datasiswaasal = [];
	$scope.reloadTable = function() {
		$rootScope.datasiswaasal =[];
		if($scope.selectData.kelas_id !==""){
			$.blockUI({});
			ApiServer.post("proktor/naikkelas/data",$scope.selectData,function(response) {
				$.unblockUI({});
				$rootScope.datasiswaasal = response.data;
			});
		}
	}
	$rootScope.datasiswatujuan =[];
	$scope.reloadTableTujuan = function() {
		$rootScope.datasiswatujuan = [];
		if($scope.tujuanData.kelas_id !==""){
			$.blockUI({});
			ApiServer.post("proktor/naikkelas/data",$scope.tujuanData,function(response) {
				$.unblockUI({});
				$rootScope.datasiswatujuan = response.data;
			});
		}
	}

	$scope.saveeditkelas = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		console.log($scope.datakelasedit);
		ApiServer.post("proktor/naikkelas/savekelas",$scope.datakelasedit,function(response) {
				$.unblockUI({ });
				if(response.success){
					$scope.getdatatingkat();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });					
						}
					})
				}				
		});
	}
	$scope.datakelasedit = {};
	$scope.dodataEdit = function() {
		$scope.datakelasedit = {};
		$scope.datakelasedit.tingkat_id = $scope.tujuanData.tingkat_id;		
		$scope.datakelasedit.tahunajaran_id = $rootScope.tujuanajaran.tahunajaran_id;;		
		//console.log($scope.dataEdit);
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
    $scope.changeTingkat = function() {
		$scope.tujuanData.tingkat_id = parseInt($scope.selectData.tingkat_id)+1+"";
		$scope.selectData.kelas_id = "";
		$scope.tujuanData.kelas_id = "";
		$rootScope.datasiswaasal = [];
		$rootScope.datasiswatujuan = [];
	}
	$scope.currPg =0;
	$scope.recordsDisplay =	0;
	$scope.currPg2 =0;
	$scope.recordsDisplay2 =	0;
	$scope.tujuanData = {};
	$scope.tujuanData.kelas_id = "";
	$scope.tujuanData.tingkat_id = "";
	$scope.tujuanData.tahunajaran_id = "";
	$scope.selectData = {};
	$scope.selectData.kelas_id = "";
	$scope.selectData.tingkat_id = "";
	$scope.selectData.tahunajaran_id = "";
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('paging', true)	
			.withOption('processing', true)
			.withOption('stateSave', true)
			.withOption('columnDefs',[
				{ orderable: false, targets: 0 }
			])
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					//console.log(pgNo);
					$scope.currPg = pgNo.start;
					$scope.recordsDisplay =	pgNo.recordsDisplay;
				}
			})
			.withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]]);
	$scope.dtOptions2 = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('paging', true)	
			.withOption('processing', true)
			.withOption('columnDefs',[
				{ orderable: false, targets: 0 }
			])
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					$scope.currPg2 = pgNo.start;
					$scope.recordsDisplay2 =	pgNo.recordsDisplay;
				}
			})
			.withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]]);

    $scope.getdatatingkat = function() {
        ApiServer.get("proktor/naikkelas/datatingkat",function(response) {
                $rootScope.asalajaran = response.asalajaran; 
                $rootScope.tujuanajaran = response.tujuanajaran; 
                $rootScope.tujuantingkat = response.tujuantingkat; 
				$rootScope.tujuankelas = response.tujuankelas; 
				$scope.tujuanData.tahunajaran_id = $rootScope.tujuanajaran.tahunajaran_id;
				$scope.selectData.tahunajaran_id = $rootScope.asalajaran.tahunajaran_id;
				$scope.lastkelas = 0;
				angular.forEach($rootScope.tujuantingkat, function(tingkat){
					$scope.lastkelas = tingkat.tingkat_id;
				})
				

        });
	}
	$scope.getdatatingkat();
        
}); 