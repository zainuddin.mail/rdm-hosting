angular.module('RdmApp').controller('semesterCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];
   $scope.savesemester = function() {
		$.blockUI({ });
		ApiServer.post("proktor/semester/save",{aktif:$rootScope.datasemester.aktif},function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
					$rootScope.getsemester();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
			}else if(response==null){
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: 'Data Gagal Disimpan'
					})
			}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
			}			
		});
	}
	
}); 