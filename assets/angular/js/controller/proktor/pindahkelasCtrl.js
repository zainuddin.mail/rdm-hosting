angular.module('RdmApp').controller('pindahkelasCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,FileUploader) {
	//$rootScope.tingkat = [];
	$scope.dtInstance = {};
	$scope.dtInstance2 = {};
	$scope.dataEdit = {}; 
	$scope.prosesNaik = function() {
		$scope.filteredArray = $scope.datasiswaasal.filter(function (siswa) {
			return (siswa.select == true);
		});
		var dataNaik = {};
		dataNaik.kelas_id = $scope.tujuanData.kelas_id;
		dataNaik.tingkat_id = $scope.selectData.tingkat_id;
		dataNaik.data = $scope.filteredArray;
		//console.log(dataNaik);
		$.blockUI({ });
		ApiServer.post("proktor/pindahkelas/save",dataNaik,function(response) {
				$.unblockUI({ });
				if(response.success){
					$scope.reloadTable(); 
					$scope.reloadTableTujuan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}

	$rootScope.datasiswaasal = {};
	$scope.reloadTable = function() {
		$rootScope.datasiswaasal = {};
		if($scope.selectData.kelas_id !==""){
			$.blockUI({});
			ApiServer.post("proktor/pindahkelas/data",$scope.selectData,function(response) {
				$.unblockUI({});
				$rootScope.datasiswaasal = response.data;
			});
		}
	}
	$rootScope.datasiswatujuan = {};
	$scope.reloadTableTujuan = function() {
		$rootScope.datasiswatujuan = {};
		if($scope.tujuanData.kelas_id !==""){
			$.blockUI({});
			ApiServer.post("proktor/pindahkelas/data",$scope.tujuanData,function(response) {
				$.unblockUI({});
				$rootScope.datasiswatujuan = response.data;
			});
		}
	}


	$scope.dodataEdit = function(jsonEdit) {
		$scope.dataEdit =  jsonEdit;		
		//console.log($scope.dataEdit);
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
    $scope.changeTingkat = function() {
		$scope.tujuanData.kelas_id = "";
		$scope.selectData.kelas_id = "";
		$rootScope.datasiswatujuan = {};
		$rootScope.datasiswaasal = {};
	}
	$scope.currPg =0;
	$scope.recordsDisplay =	0;
	$scope.currPg2 =0;
	$scope.recordsDisplay2 =	0;
	$scope.tujuanData = {};
	$scope.tujuanData.kelas_id = "";
	$scope.tujuanData.tingkat_id = "";
	$scope.tujuanData.tahunajaran_id = "";
	$scope.selectData = {};
	$scope.selectData.kelas_id = "";
	$scope.selectData.tingkat_id = "";
	$scope.selectData.tahunajaran_id = "";
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('paging', true)	
			.withOption('stateSave', true)
			.withOption('processing', true)
			.withOption('columnDefs',[
				{ orderable: false, targets: 0 }
			])
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					//console.log(pgNo);
					$scope.currPg = pgNo.start;
					$scope.recordsDisplay =	pgNo.recordsDisplay;
				}
			})
			.withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]]);
	$scope.dtOptions2 = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('paging', true)	
			.withOption('processing', true)
			.withOption('columnDefs',[
				{ orderable: false, targets: 0 }
			])
			.withOption('drawCallback', function(settings) {
				if(settings.aoData.length > 0) {
					var api = this.api();
					var pgNo = api.page.info();
					$scope.currPg2 = pgNo.start;
					$scope.recordsDisplay2 =	pgNo.recordsDisplay;
				}
			})
			.withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]]);

    $scope.getdatatingkat = function() {
        ApiServer.get("proktor/pindahkelas/datatingkat",function(response) {
                $rootScope.asalajaran = response.asalajaran; 
                $rootScope.tujuanajaran = response.tujuanajaran; 
                $rootScope.asaltingkat = response.asaltingkat; 
				$rootScope.asalkelas = response.asalkelas; 
				$scope.tujuanData.tahunajaran_id = $rootScope.tujuanajaran.tahunajaran_id;
				$scope.selectData.tahunajaran_id = $rootScope.asalajaran.tahunajaran_id;

        });
	}
	$scope.getdatatingkat();
        
}); 