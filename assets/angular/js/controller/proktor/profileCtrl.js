angular.module('RdmApp').controller('profileCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	$rootScope.password = {};	
	function clearInputFile(f){
		if(f.value){
			try{
				f.value = ''; //for IE11, latest Chrome/Firefox/Opera...
			}catch(err){ }
			if(f.value){ //for IE5 ~ IE10
				var form = document.createElement('form'),
					parentNode = f.parentNode, ref = f.nextSibling;
				form.appendChild(f);
				form.reset();
				parentNode.insertBefore(f,ref);
			}
		}
	}
	$scope.hapusLogo = function(){
		$.blockUI({ });
		ApiServer.get("proktor/hapuslogo",function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getprofile(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Logo berhasil dihapus',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.savePimpinan = function(){
		$.blockUI({ });
		ApiServer.post("proktor/savepimpinan",$rootScope.pimpinan,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getprofile(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.syncProfile = function(){
		$.blockUI({ });
		ApiServer.post("proktor/syncprofile",$rootScope.pimpinan,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getprofile(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.savePassword = function(){
		$.blockUI({ });
		ApiServer.post("proktor/ubahpassword",$rootScope.password,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.password.password = ""; 
					$rootScope.password.konfirmasi = ""; 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
	$scope.browseimage = function(){
		$("#imageFile").click();
	}	
	$scope.$on("pictureInsert", function(evt,data){ 
		$("#imageFile").val(null);
		$rootScope.profile.lembaga_foto =data;	
	});
}); 
angular.module('RdmApp').directive('myDirective', function (httpPostFactory) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
				$.blockUI({ });
                var formData = new FormData();
                formData.append('file', element[0].files[0]);
                var url = element[0].getAttribute("data-url");
                var container = element[0].getAttribute("image-container");
                httpPostFactory(url, formData, function (callback) {
				   // recieve image name to use in a ng-src 
				   $.unblockUI({ });
				   if(callback && callback.success){
						scope.$emit('pictureInsert',callback.data);	
											
				   }else{
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: callback.data
						})
				   }
				   
                });
            });

        }
    };
});