angular.module('RdmApp').controller('prestasiCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$scope.dataEdit = {};
    $scope.dodataTambah = function() {
        $scope.dataEdit ={};
        $scope.dataEdit.edit = false;
        $(".edit-modal").modal("show");      
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
 
    }
    $scope.dodataEdit = function(siswa_id,data) {
        $scope.dataEdit ={};
        $scope.dataEdit.edit = true;
        $scope.dataEdit.siswa_id = siswa_id;
        $scope.dataEdit.prestasi_id = data.prestasi_id;
        $scope.dataEdit.prestasi_keterangan = data.prestasi_keterangan;
        $scope.dataEdit.prestasi_nama = data.prestasi_nama;
        $(".edit-modal").modal("show");       
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

    }
    $scope.savedataEdit = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/walas/saveprestasi/"+$rootScope.walas,$scope.dataEdit,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getwalassiswa();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {  
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	
	$scope.dodataDelete = function(data) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Yes, delete it!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus data ini?",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("guru/walas/deleteprestasi",data,function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){
							$rootScope.getwalassiswa();
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})

	}


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	  .withOption('stateSave', true)
	  .withOption('paging', false)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	.withOption('stateSave', true)
	.withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);


}); 