angular.module('RdmApp').controller('sikapsosCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai ={};
	
	$rootScope.knilai.sikapAll ="";
    $scope.setsikapAll = function(sikapAll) {
		console.log(sikapAll);
		$rootScope.knilai.tambahsikap ={};
		angular.forEach($rootScope.datasikapsos, function(siswa){
			if(sikapAll!== null && sikapAll!== ""){
				$rootScope.knilai.tambahsikap[siswa.siswa_id] = sikapAll ;
			}
        });
	}
    $scope.editKompnilai = function(edit) {
        //console.log(edit);
		$rootScope.knilai.sikapAll ="";
        $rootScope.knilai.edit = true;
        $rootScope.knilai.jenisnilai_id = edit.jenisnilai_id;
        $rootScope.knilai.komponennilai_id = edit.komponennilai_id;
        $rootScope.knilai.tambahsikap = {};
        $scope.FilSikap = $rootScope.jenisnilaisos.filter(function (data) {
            return (data.jenisnilai_id == edit.jenisnilai_id);
        });
        //console.log($scope.FilSikap);   
        $rootScope.knilai.tambahsikap = $scope.FilSikap[0].datanilai;
        $(".edit-modal").modal("show");      
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

    }
    $scope.tambahharian = function() {
		$rootScope.knilai.sikapAll ="";
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

        
    }
    $scope.UploadTemplate = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".upload-modal").modal("show");
        
    }
    $scope.deleteKompnilai = function(data) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Ya, Hapus!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus nilai ini? Nilai yang sudah di hapus tidak bisa dikembalikan lagi.",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("guru/sikap/deletesikapwalas",data,function(response) {
					//console.log(response);
						$.unblockUI({ });
                        $rootScope.getSikapsos();
						if(response.success){
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})


    }
    $scope.savedatasikap = function() {
        //console.log($rootScope.knilai);

		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/sikap/savesikapwalas/"+$rootScope.walas,$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getSikapsos();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {    
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	.withOption('stateSave', true)
    .withOption('paging', false)	
	.withOption('responsive', true)
	.withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
	.withOption('stateSave', true)
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);

	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'guru/sikap/importnilai' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$rootScope.getSikapsos();
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'guru/sikap/importnilaiwalas/sos/'+$rootScope.walas;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};

}); 