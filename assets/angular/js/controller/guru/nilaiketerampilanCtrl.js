angular.module('RdmApp').controller('nilaiketerampilanCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai ={};
    $scope.tambahharian = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

        
    }
    $scope.deleteKompnilai = function(data) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Ya, Hapus!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus nilai ini? Nilai yang sudah di hapus tidak bisa dikembalikan lagi.",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("guru/keterampilan/deletenilai",data,function(response) {
					//console.log(response);
						$.unblockUI({ });
                        $rootScope.getketerampilan(); 
						if(response.success){
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})


    }
    $scope.savedataharian = function() {
        //console.log($rootScope.knilai);
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/keterampilan/nilaiket",$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getketerampilan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	.withOption('stateSave', true)
    .withButtons([
          {
              extend:    'copy',
              text:      '<i class="fa fa-files-o"></i> Copy',
              titleAttr: 'Copy'
          },
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
              download: 'open',
              exportOptions: {
                  columns: [ 0,1,2,3,]
                },
              titleAttr: 'Print',
              customize: function (doc){
                  $rootScope.customepdf(doc,'Absen Siswa','RDM')
              }
          },
          {
              extend:    'excelHtml5',
              text:      '<i class="fa fa-file-text-o"></i> Excel',
              exportOptions: {
                  columns: [ 0,2,3,]
                },
              titleAttr: 'Excel'
          }
      ])
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);

	$scope.UploadTemplate = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".upload-modal").modal("show");
        
    }
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'guru/keterampilan/importnilai' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$rootScope.getketerampilan(); 
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'guru/keterampilan/importnilai/praktek/'+$rootScope.selectKelas;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};

}); 