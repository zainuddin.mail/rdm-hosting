angular.module('RdmApp').controller('keterampilanCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai = {};
	$scope.editKompnilai = function(edit) {
        $rootScope.knilai.edit = true;
        $rootScope.knilai.komponennilai_id = edit.komponennilai_id;
        $rootScope.knilai.komponennilai_materi = edit.komponennilai_materi;
        $rootScope.knilai.komponennilai_nama = edit.nama;
        $rootScope.knilai.uk = {};
        $rootScope.knilai.pro = {};
        $rootScope.knilai.por = {};
        angular.forEach($rootScope.dataketerampilan, function(siswa){
			$scope.FilNilai = siswa.finalkd.filter(function (data) {
				return (data.nama == edit.nama);
			});
			console.log("Finalkd:");
			console.log(siswa.finalkd);
			console.log("Edit:");
			console.log(edit);
			if($scope.FilNilai.length > 0){
				if(parseInt($scope.FilNilai[0].uk) > 0 ) $rootScope.knilai.uk[siswa.siswa_id] = parseInt($scope.FilNilai[0].uk) ;
				else $rootScope.knilai.uk[siswa.siswa_id] = "" ;
				if(parseInt($scope.FilNilai[0].pro) > 0 )  $rootScope.knilai.pro[siswa.siswa_id] = parseInt($scope.FilNilai[0].pro) ;
				else $rootScope.knilai.pro[siswa.siswa_id] = "" ;
				if(parseInt($scope.FilNilai[0].por) > 0 )  $rootScope.knilai.por[siswa.siswa_id] = parseInt($scope.FilNilai[0].por) ;
				else $rootScope.knilai.por[siswa.siswa_id] = "" ;
			}else{
				$rootScope.knilai.uk[siswa.siswa_id] = "" ;
				$rootScope.knilai.pro[siswa.siswa_id] = "" ;
				$rootScope.knilai.por[siswa.siswa_id] = "" ;
			}

        });
		console.log($rootScope.knilai);
        $(".edit-modal").modal("show");      
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

    }
    $scope.tambahharian = function() {
        $rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

        
	}
    $scope.savedataharian = function() {
        //console.log($rootScope.knilai);

		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/keterampilan/saveketall",$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getketerampilan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}

    $scope.kirimNilai = function() {    
        $rootScope.knilai.kirimnilai = [];
        $i = 0;
        $rootScope.knilai.selectKelas = $rootScope.selectKelas;
		var validnilai = false;
		var namanonvalid = "";
        angular.forEach($rootScope.dataharian, function(siswa){
            var datakirim = {};
            datakirim.siswa_id = siswa.siswa_id;
            datakirim.rapor = siswa.rapork;
			if(parseInt(siswa.rapork) ){
				validnilai = true;
			}
            datakirim.predikat = siswa.predikatk;
            datakirim.desc = siswa.desck;
            $rootScope.knilai.kirimnilai[$i] = datakirim;
            $i++;
        });   
		if(validnilai){
			//console.log($rootScope.knilai);
			$.blockUI({ });
			ApiServer.post("guru/keterampilan/kirimnilai",$rootScope.knilai,function(response) {
				//console.log(response);
					$.unblockUI({ });
					$rootScope.getketerampilan(); 
					$rootScope.getdatakelas();
					if(response.success){
						Swal.fire({
							type: 'success',
							title: 'Berhasil',
							showConfirmButton: false,
							html: 'Data Berhasil Dikirim',
							timer: 1000,
						})
					}else{
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: response.message
						})
					}				
			});
		}else{
			Swal.fire({
				type: 'warning',
				title: 'Perhatian!!!',
				showConfirmButton: true,
				html:"Nilai rapor masih kosong semua, nilai tidak bisa di kirim."
			})
		}
	}
   
    $scope.batalNilai = function() {    
        $rootScope.knilai.kirimnilai = [];
        $rootScope.knilai.selectKelas = $rootScope.selectKelas;  
		$.blockUI({ });
		ApiServer.post("guru/keterampilan/batalnilai",$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				$rootScope.getketerampilan(); 
				$rootScope.getdatakelas();
				if(response.success){
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Dikirim',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
		
    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	.withOption('stateSave', true)
    .withButtons([
          {
              extend:    'copy',
              text:      '<i class="fa fa-files-o"></i> Copy',
              titleAttr: 'Copy'
          },
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
              download: 'open',

              titleAttr: 'Print',
              customize: function (doc){
                  $rootScope.customepdf(doc,'Nilai Pengetahuan','Rapor Digital Madrasah')
              }
          },
          {
              extend:    'excelHtml5',
              text:      '<i class="fa fa-file-text-o"></i> Excel',
              exportOptions: {
                  columns: [ 0,2,3,]
                },
              titleAttr: 'Excel'
          }
      ])
      .withOption('responsive', true)
      .withOption('paging', false)	
    .withOption('processing', false);

}); 