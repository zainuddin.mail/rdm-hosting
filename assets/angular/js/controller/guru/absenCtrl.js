angular.module('RdmApp').controller('absenCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
    $scope.dtInstance = {};
	$rootScope.knilai ={};
	$rootScope.sakitAll ={};
	$rootScope.ijinAll ={};
	$rootScope.aplaAll ={};
    $scope.setsakitAll = function(sakitAll) {
        if(sakitAll !== null){
            angular.forEach($rootScope.walassiswa, function(siswa){
                siswa.absen.sakit = sakitAll;
            });
        }
    }
    $scope.setijinAll = function(ijinAll) {
        console.log(ijinAll);
        if(ijinAll !== null){
            angular.forEach($rootScope.walassiswa, function(siswa){
                siswa.absen.ijin = ijinAll;
            });
        }
    }
    $scope.setalpaAll = function(alpaAll) {
        if(alpaAll !== null){
            angular.forEach($rootScope.walassiswa, function(siswa){
                siswa.absen.alpa = alpaAll;
            });
        }
    }
    $scope.tambahpas = function() {
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");       
        $('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

    }
    $scope.simpanAbsen = function() {
         $rootScope.knilai ={};
         $rootScope.knilai.absen = [];
        angular.forEach($rootScope.walassiswa, function(siswa){
             var absen = {};
             absen.siswa_id = siswa.siswa_id;
             absen.sakit = siswa.absen.sakit;
             absen.ijin = siswa.absen.ijin;
             absen.alpa = siswa.absen.alpa;
            $rootScope.knilai.absen.push(absen);
        });
        //console.log($rootScope.knilai);
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/walas/saveabsen/"+$rootScope.walas,$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getwalassiswa();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {    
							$(".edit-modal").modal("show");
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('stateSave', true)
    .withOption('paging', false)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('stateSave', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);


}); 