angular.module('RdmApp').controller('forumCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,nowTime) {
	//$rootScope.dashboard = [];
	// Variables
	$scope.nowTime = nowTime;
    $scope.row = 0;
    $scope.rowperpage = 3;
	$scope.newPost = {};
	$scope.newKomen = {};
	$scope.newPost.image = "";
	$scope.newPost.link = "";
	$scope.newPost.text = "";
	$scope.newPost.type = 1;
    $scope.busy = false;
    $scope.loading = false;       
    // Fetch data
    $scope.setType = function(type){
		$scope.newPost.type = type;
	}
    $scope.saveComent = function(data,comment){
		$.blockUI({ });
		$scope.newKomen = {};
		$scope.newKomen.forumrespon_komentar = comment;
		$scope.newKomen.forum_id = data.forum_id;
		ApiServer.post("guru/forum/savekomen",$scope.newKomen,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.updatePosts(data);
					$scope.newPost.image = "";
					$scope.newPost.link = "";
					$scope.newPost.text = "";
					$scope.newPost.type = 1;
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
    $scope.browseimage = function(){
		$("#imageFile").click();
	}
    $scope.removeImage = function(){
		$scope.newPost.image ="";
	}
	$scope.sendPost = function() {
		$.blockUI({ });
		ApiServer.post("guru/forum/save",$scope.newPost,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.getnewPosts();
					$scope.newPost.image = "";
					$scope.newPost.link = "";
					$scope.newPost.text = "";
					$scope.newPost.type = 1;
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
    $scope.getnewPosts = function(){
		if($scope.posts.length > 0){
			forum_id = $rootScope.posts[0].forum_id;
		}
		ApiServer.get("guru/forum/newdata/"+forum_id,function(response) {
			if(response.success){
				$scope.row+=$scope.rowperpage;
				$scope.$apply(function(){      
					angular.forEach(response.data,function(item) {
						$rootScope.posts.unshift(item);
					});
					setTimeout(function() {
						$('.lazy').Lazy();			
					},1000);					
				});
				
			}
		});
	}
    $scope.updatePosts = function(dt){
		$.blockUI({ });
		forum_id = dt.forum_id;
		ApiServer.get("guru/forum/getupdatedata/"+forum_id,function(response) {
			$.unblockUI({ });
			if(response.success){
				dt = response.data;
				const indexOfItemInArray = $rootScope.posts.findIndex(q => q.forum_id === response.data.forum_id);
				$rootScope.posts[indexOfItemInArray] = response.data;
			}
		});
	}
    $scope.getPosts = function(){
		if ($scope.busy) return;
		$scope.loading = true;
		$scope.busy = true;
		forum_id ="0";
		if($rootScope.posts.length > 0){
			forum_id = $rootScope.posts[$rootScope.posts.length-1].forum_id;
		}
		ApiServer.get("guru/forum/data/"+forum_id,function(response) {
			if(response.success){
				$scope.row+=$scope.rowperpage;
				$scope.$apply(function(){      
					angular.forEach(response.data,function(item) {
						$rootScope.posts.push(item);
					});
					$scope.busy = false;
					$scope.loading = false;
					setTimeout(function() {
						$('.lazy').Lazy();			
					},1000);					
				});
				
			}else{
				$scope.busy = false;
				$scope.loading = false;
			}
		});

	}
    $scope.$on("pictureInsert", function(evt,data){ 
		$scope.newPost.image =data;
		//console.log($scope.newPost.image);
	});
	if($rootScope.posts.length == 0){
		$scope.getPosts();
	}
	$("#postText").focus();
}); 
angular.module('RdmApp').directive('myDirective', function (httpPostFactory) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
				$.blockUI({ });
                var formData = new FormData();
                formData.append('file', element[0].files[0]);
                var url = element[0].getAttribute("data-url");
                var container = element[0].getAttribute("image-container");
                httpPostFactory(url, formData, function (callback) {
				   // recieve image name to use in a ng-src 
				   $.unblockUI({ });
				   if(callback && callback.success){
						scope.$emit('pictureInsert',callback.data);						
				   }else{
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: callback.data
						})
				   }
				   
                });
            });

        }
    };
});