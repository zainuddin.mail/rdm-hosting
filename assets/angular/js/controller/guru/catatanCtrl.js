angular.module('RdmApp').controller('catatanCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai ={};
    $scope.tambahpas = function() {
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");       
    }
    $scope.simpanCatatan = function() {
         $rootScope.knilai ={};
         $rootScope.knilai.absen = [];
        angular.forEach($rootScope.walassiswa, function(siswa){
             var absen = {};
             absen.siswa_id = siswa.siswa_id;
             absen.catatan = siswa.absen.catatan;
            $rootScope.knilai.absen.push(absen);
        });
        //console.log($rootScope.knilai);
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/walas/savecatatan/"+$rootScope.walas,$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getwalassiswa();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {    
							$(".edit-modal").modal("show");
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('stateSave', true)
    .withOption('paging', false)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);


}); 