angular.module('RdmApp').controller('nilaiCtrl', function ($cookies,$scope,$rootScope,$stateParams,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
    $scope.dtInstance = {};
	$rootScope.walasnilai = {};
	$rootScope.dataketerampilan = [];
	$rootScope.bobot = [];
	$rootScope.kkmdata = [];	
    $scope.$on("KelasChange", function(evt,data){ 
		//console.log(data);
        if(data.length > 0){
            $rootScope.getwalasnilai();
        }
    });
	$rootScope.locknilai = function(status) {
        if($rootScope.walas !==''){
            $.blockUI({});
            ApiServer.post("guru/walas/locknilai/"+ $rootScope.walas,{status:status},function(response) {
                $.unblockUI({}); 
                $rootScope.getwalasnilai();
		    });
        }
	}
	$rootScope.getwalasnilai = function() {
        if($rootScope.walas !==''){
            $.blockUI({});
            ApiServer.get("guru/walas/datanilai/"+ $rootScope.walas,function(response) {
                $.unblockUI({}); 
				$rootScope.walasnilai = response.data; 
				$rootScope.walaskirim = response.kirim; 
				$rootScope.nilailock = response.nilailock; 
		});
        }
	}
	$rootScope.getwalasnilai();	

    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('stateSave', true)
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
}); 