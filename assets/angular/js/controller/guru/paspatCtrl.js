angular.module('RdmApp').controller('paspatCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai ={};
	$scope.setallNilai = function(allNilai) {
		if(allNilai !==null && allNilai !==""){
			angular.forEach($rootScope.datasiswa, function(siswa){
				siswa.paspat = allNilai;
			});
		}
	}
    $scope.tambahpas = function() {
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");       
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

    }
    $scope.simpanPasPat = function() {
         $rootScope.knilai ={};
         $rootScope.knilai.nilaipas = {};
        angular.forEach($rootScope.datasiswa, function(siswa){
            $rootScope.knilai.nilaipas[siswa.siswa_id] = siswa.paspat;
        });
        //console.log($rootScope.knilai);
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/pengetahuan/savepaspat/"+$rootScope.selectKelas,$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getharian(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)

	  .withOption('stateSave', true)
	  .withOption('paging', false)	
    .withOption('tabIndex', -1)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);

    
    $scope.UploadTemplate = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".upload-modal").modal("show");
        
    }
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'guru/pengetahuan/importnilai' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$rootScope.getharian(); 
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'guru/pengetahuan/importnilai/paspat/'+$rootScope.selectKelas;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};

}); 