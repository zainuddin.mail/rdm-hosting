angular.module('RdmApp').controller('walasCtrl', function ($cookies,$scope,$rootScope,$stateParams,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	$rootScope.dataharian = [];
	$rootScope.dataketerampilan = [];
	$rootScope.bobot = [];
	$rootScope.kkmdata = [];	
	$scope.$on("KelasChange", function(evt,data){ 
		//console.log(data);
        if(data.length > 0){
            $rootScope.getwalassiswa();
        }
    });
	$rootScope.getwalassiswa = function() {
        if($rootScope.walas !==''){
			$.blockUI({ });
            ApiServer.get("guru/walas/datasiswa/"+ $rootScope.walas,function(response) {
				$.unblockUI({ });
				$rootScope.getSikapsos();
				$rootScope.getSikapsspi();
				$rootScope.tingkatwalas = response.tingkat; 
				$rootScope.walassiswa = response.data; 
				$rootScope.dataabsen = response.dataabsen; 
				$rootScope.dataprestasi = response.dataprestasi; 
				angular.forEach($rootScope.walassiswa, function(siswa){
					if($rootScope.dataabsen[siswa.siswa_id] !== undefined){
						siswa.absen = $rootScope.dataabsen[siswa.siswa_id];
					}else{
						siswa.absen ={};
					}
					if($rootScope.dataprestasi[siswa.siswa_id] !== undefined){
						siswa.prestasi = $rootScope.dataprestasi[siswa.siswa_id];
					}else{
						siswa.prestasi ={};
					}
				});
				//console.log($rootScope.walassiswa);
		});
        }
	}
	$rootScope.getwalassiswa();	
	$rootScope.dataRapor = {};	
    $rootScope.getharian = function() {
		ApiServer.get("guru/pengetahuan/harian",function(response) {
				$rootScope.dataharian = []; 
				$rootScope.datakomponen = response.komponen; 
				$rootScope.datanilai = response.datanilai; 
				$rootScope.datapas = response.datapas; 
				$rootScope.dataRapor = response.datarapor; 
				console.clear();	
				//console.log("$rootScope.dataRapor");	
				//console.log($rootScope.dataRapor);	
                angular.forEach($rootScope.datasiswa, function(siswa){
					var nilaimax = 0;
					var descmax = "";
					var descmin = "";
					var nilaimin = 100;
                    if($rootScope.datanilai[siswa.siswa_id] !== undefined){
                        siswa.nilai = $rootScope.datanilai[siswa.siswa_id];
						if($rootScope.datapas[siswa.siswa_id] !== undefined){
							siswa.paspat = parseInt($rootScope.datapas[siswa.siswa_id]);
						}else{
							siswa.paspat = 0;
							if($rootScope.bobot.bobotkelas['paspat'] > 0){
								nilaimin = 0;
							}
						}
						var nilaiharian = 0;
						angular.forEach($rootScope.datakomponen, function(komp){
							var nilaikomp;
							if(siswa.nilai[komp.komponennilai_id] !== undefined){
								nilaikomp = parseInt(siswa.nilai[komp.komponennilai_id]);
								 
							}else{
								nilaikomp = 0;
							}
							if(nilaikomp > nilaimax){
								nilaimax = nilaikomp;
								descmax = komp.komponennilai_materi;
							}
							if(nilaikomp < nilaimin){
								nilaimin = nilaikomp;
								descmin = komp.komponennilai_materi;
							}
							nilaiharian +=nilaikomp;

						});
						siswa.nilaiharian = Math.round((nilaiharian/$rootScope.datakomponen.length),0);
					}else{
                        siswa.nilai = null;
						siswa.nilaiharian = 0;
						siswa.paspat = 0;
						if($rootScope.bobot.bobotkelas['harian'] > 0){
							nilaimin = 0;
						} 
						if($rootScope.bobot.bobotkelas['paspat'] > 0){
							nilaimin = 0;
						}
                    }
					if($rootScope.dataRapor[siswa.siswa_id] !== undefined){
						siswa.rapor = $rootScope.dataRapor[siswa.siswa_id].rapor_nilai;
						siswa.predikat = $rootScope.dataRapor[siswa.siswa_id].rapor_predikat;
						siswa.desc = $rootScope.dataRapor[siswa.siswa_id].rapor_deskripsi;
					}else{
						if(siswa.nilaiharian + siswa.paspat > 0){
							siswa.rapor = Math.round((((siswa.nilaiharian * $rootScope.bobot.bobotkelas['harian']) + (siswa.paspat * $rootScope.bobot.bobotkelas['paspat']))/( $rootScope.bobot.bobotkelas['harian'] + $rootScope.bobot.bobotkelas['paspat'])),2);
						}else{
							siswa.rapor = 0;
						}
						$scope.FilPredikat = $rootScope.kkmdata.kkmgrade.filter(function (data) {
							return (parseInt(data.kkmgrade_min) <= siswa.rapor && parseInt(data.kkmgrade_max) >= siswa.rapor);
						});
						//console.log($scope.FilPredikat);
						if($scope.FilPredikat.length > 0){
							siswa.predikat = $scope.FilPredikat[0].kkmgrade_kode;
							var kkm = parseInt($rootScope.kkmdata.kkmtingkat_nilai);
							var desc = "";
							if(siswa.predikat == "A"){
								desc = "sangat baik";
	
							}else if(siswa.predikat == "B"){
								desc = "baik";
							}else if(siswa.predikat == "C"){
								desc = "cukup baik";
							}else{
								desc = "kurang baik";
							}
							if( kkm <= nilaimin && kkm <= nilaimax){
								siswa.desc ="Memiliki kompetensi dari semua materi yang " + desc + " terutama menguasai "+ descmax;
							}else if(kkm > nilaimin && nilaimin > 0 && kkm <= nilaimax){
								siswa.desc ="Memiliki kompetensi dari semua materi yang " + desc + " terutama menguasai "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
							}else{
								siswa.desc ="Perlu pebaikan kompetensi pada seluruh materi";
							}
							if(siswa.rapor == 0){
								siswa.desc = "";
							}
						}else{
							siswa.predikat = "-";
							siswa.desc = "";
						}
					}

					////console.log(siswa);
                    $rootScope.dataharian.push(siswa);
                })
		});
	}
    $rootScope.getketerampilan = function() {
		ApiServer.get("guru/keterampilan/dataketerampilan",function(response) {
				$rootScope.dataketerampilan = []; 
				$rootScope.komponenuk = response.komponenuk; 
				$rootScope.datauk = response.datauk; 
				$rootScope.komponenpro = response.komponenpro; 
				$rootScope.datapro = response.datapro; 
				$rootScope.komponenpor = response.komponenpor; 
				$rootScope.datapor = response.datapor; 
                angular.forEach($rootScope.datasiswa, function(siswa){
					var nilaimax = 0;
					var descmax = "";
					var descmin = "";
					var nilaimin = 100;
                    if($rootScope.datauk[siswa.siswa_id] !== undefined){
                        siswa.nilaiuk = $rootScope.datauk[siswa.siswa_id];
						var nilaiuk = 0;
						angular.forEach($rootScope.komponenuk, function(komp){
							var nilaikomp;
							if(siswa.nilaiuk[komp.komponennilai_id] !== undefined){
								nilaikomp = parseInt(siswa.nilaiuk[komp.komponennilai_id]);
							}else{
								nilaikomp = 0;
							}
							if($rootScope.bobot.bobotkelas['praktek'] > 0){
								if(nilaikomp > nilaimax){
									nilaimax = nilaikomp;
									descmax = komp.komponennilai_materi;
								}
								if(nilaikomp < nilaimin){
									nilaimin = nilaikomp;
									descmin = komp.komponennilai_materi;
								}
							}
							nilaiuk +=nilaikomp;
							//console.log(nilaiuk);
						});
						siswa.uk = Math.round((nilaiuk/$rootScope.komponenuk.length),0);
					}else{
                        siswa.nilaiuk = null;
						siswa.uk = 0;
						if($rootScope.bobot.bobotkelas['praktek'] > 0){
							nilaimin = 0;
						}
                    }
                    if($rootScope.datapro[siswa.siswa_id] !== undefined){
                        siswa.nilaipro = $rootScope.datapro[siswa.siswa_id];
						var nilaipro = 0;
						angular.forEach($rootScope.komponenpro, function(komp){
							var nilaikomp;
							if(siswa.nilaipro[komp.komponennilai_id] !== undefined){
								nilaikomp = parseInt(siswa.nilaipro[komp.komponennilai_id]);
							}else{
								nilaikomp = 0;
							}
							if($rootScope.bobot.bobotkelas['proyek'] > 0){
								if(nilaikomp > nilaimax){
									nilaimax = nilaikomp;
									descmax = komp.komponennilai_materi;
								}
								if(nilaikomp < nilaimin){
									nilaimin = nilaikomp;
									descmin = komp.komponennilai_materi;
								}
							}
							nilaipro +=nilaikomp;
						});
						siswa.pro = Math.round((nilaipro/$rootScope.komponenpro.length),0);
					}else{
                        siswa.nilaipro = null;
						siswa.pro = 0;
						if($rootScope.bobot.bobotkelas['proyek'] > 0){
							nilaimin = 0;
						} 
					}
                    if($rootScope.datapor[siswa.siswa_id] !== undefined){
                        siswa.nilaipor = $rootScope.datapor[siswa.siswa_id];
						var nilaipor = 0;
						angular.forEach($rootScope.komponenpor, function(komp){
							var nilaikomp;
							if(siswa.nilaipor[komp.komponennilai_id] !== undefined){
								nilaikomp = parseInt(siswa.nilaipor[komp.komponennilai_id]);
							}else{
								nilaikomp = 0;
							}
							if($rootScope.bobot.bobotkelas['porto'] > 0){
								if(nilaikomp > nilaimax){
									nilaimax = nilaikomp;
									descmax = komp.komponennilai_materi;
								}
								if(nilaikomp < nilaimin){
									nilaimin = nilaikomp;
									descmin = komp.komponennilai_materi;
								}
							}
							nilaipor +=nilaikomp;

						});
						siswa.por = Math.round((nilaipor/$rootScope.komponenpor.length),0);
					}else{
                        siswa.nilaipor = null;
						siswa.por = 0;
						if($rootScope.bobot.bobotkelas['porto'] > 0){
							nilaimin = 0;
						} 
					}
					if(siswa.uk + siswa.pro + siswa.por > 0){
						siswa.rapork = Math.round((((siswa.uk * $rootScope.bobot.bobotkelas['praktek']) + (siswa.pro * $rootScope.bobot.bobotkelas['proyek'])  + (siswa.por * $rootScope.bobot.bobotkelas['porto'])) / ( $rootScope.bobot.bobotkelas['praktek'] + $rootScope.bobot.bobotkelas['proyek'] + $rootScope.bobot.bobotkelas['porto'])),2);
					}else{
						siswa.rapork = 0;
					}
					$scope.FilPredikat = $rootScope.kkmdata.kkmgrade.filter(function (data) {
						return (parseInt(data.kkmgrade_min) <= siswa.rapork && parseInt(data.kkmgrade_max) >= siswa.rapork);
					});
					////console.log($scope.FilPredikat);
					if($scope.FilPredikat.length > 0){
						siswa.predikatk = $scope.FilPredikat[0].kkmgrade_kode;
						var kkm = parseInt($rootScope.kkmdata.kkmtingkat_nilai);
						var desc = "";
						if(siswa.predikatk == "A"){
							desc = "sangat baik";

						}else if(siswa.predikatk == "B"){
							desc = "baik";
						}else if(siswa.predikatk == "C"){
							desc = "cukup baik";
						}else{
							desc = "kurang baik";
						}
						if( kkm <= nilaimin && kkm <= nilaimax){
							siswa.desck ="Memiliki kompetensi dari semua materi yang " + desc + " terutama menguasai "+ descmax;
						}else if(kkm > nilaimin && nilaimin > 0 && kkm <= nilaimax){
							siswa.desck ="Memiliki kompetensi dari semua materi yang " + desc + " terutama menguasai "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
						}else{
							siswa.desck ="Perlu pebaikan kompetensi pada seluruh materi";
						}
						if(siswa.rapork == 0){
							siswa.desck = "";
						}
					}else{
						siswa.predikatk = "-";
					}
					//console.log(siswa);
                    $rootScope.dataketerampilan.push(siswa);
                })
		});
	}
	$rootScope.getSikapsos =function() {
		$.blockUI({ });
		ApiServer.get("guru/sikap/datasikapsos/"+$rootScope.walas,function(response) {
				$.unblockUI({ });
				$rootScope.datasikapsos = []; 
				$rootScope.datakomponensikapsos = response.komponen; 
				$rootScope.datanilaisikapsos = response.datanilai; 
				$rootScope.jenisnilaisos = response.jenisnilai; 
                angular.forEach($rootScope.walassiswa, function(siswa){
						$rootScope.datasikapsos.push(siswa);
				});
				$rootScope.jnilaisos = 0;
                angular.forEach($rootScope.jenisnilaisos, function(jenis){
					////console.log("Jenis length:"+jenis.cnilai);
					if(jenis.cnilai > 0 ) $rootScope.jnilaisos++;
				});
				//////console.log("JJenis = "+$rootScope.jnilai);
		});
	}
	$rootScope.getSikapsspi =function() {
		$.blockUI({ });
		ApiServer.get("guru/sikap/datasikapspi/"+$rootScope.walas,function(response) {
				$.unblockUI({ });
				$rootScope.datasikapspi = []; 
				$rootScope.datakomponensikapspi = response.komponen; 
				$rootScope.datanilaisikapspi = response.datanilai; 
				$rootScope.jenisnilaispi = response.jenisnilai; 
                angular.forEach($rootScope.walassiswa, function(siswa){
						$rootScope.datasikapspi.push(siswa);
				});
				$rootScope.jnilaispi = 0;
                angular.forEach($rootScope.jenisnilaispi, function(jenis){
					////console.log("Jenis length:"+jenis.cnilai);
					if(jenis.cnilai > 0 ) $rootScope.jnilaispi++;
				});
				//////console.log("JJenis = "+$rootScope.jnilai);
		});
	}

}); 