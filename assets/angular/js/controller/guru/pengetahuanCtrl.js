angular.module('RdmApp').controller('pengetahuanCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai = {};
    $scope.kirimNilai = function() {    
        $rootScope.knilai.kirimnilai = [];
        $i = 0;
        var validnilai = false;
		var namanonvalid = "";
        angular.forEach($rootScope.dataharian, function(siswa){
            var datakirim = {};
            datakirim.siswa_id = siswa.siswa_id;
            datakirim.rapor = siswa.rapor;
            if(parseInt(siswa.rapor) ){
				validnilai = true;
			}
            datakirim.predikat = siswa.predikat;
            datakirim.desc = siswa.desc;
            $rootScope.knilai.selectKelas = $rootScope.selectKelas;
            $rootScope.knilai.kirimnilai[$i] = datakirim;
            $i++;
        });   
        if(validnilai){
            //console.log($rootScope.knilai);
            $.blockUI({ });
            ApiServer.post("guru/pengetahuan/kirimnilai",$rootScope.knilai,function(response) {
                //console.log(response);
                    $.unblockUI({ });
                    $rootScope.getharian(); 
                    $rootScope.getdatakelas();
                    if(response.success){
                        Swal.fire({
                            type: 'success',
                            title: 'Berhasil',
                            showConfirmButton: false,
                            html: 'Data Berhasil Dikirim',
                            timer: 1000,
                        })
                    }else{
                        Swal.fire({
                            type: 'error',
                            title: 'Gagal',
                            showConfirmButton: true,
                            html: response.message
                        })
                    }				
            });
        }else{
            Swal.fire({
                type: 'warning',
                title: 'Perhatian!!!',
                showConfirmButton: true,
                html:"Nilai rapor masih kosong semua, nilai tidak bisa di kirim."
            })
        }
	}
   
    $scope.batalNilai = function() {    
        $rootScope.knilai.kirimnilai = [];
        $rootScope.knilai.selectKelas = $rootScope.selectKelas;  
		$.blockUI({ });
		ApiServer.post("guru/pengetahuan/batalnilai",$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				$rootScope.getharian(); 
                $rootScope.getdatakelas();
				if(response.success){
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Dikirim',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
   

    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withButtons([
          {
              extend:    'copy',
              text:      '<i class="fa fa-files-o"></i> Copy',
              titleAttr: 'Copy'
          },
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
              download: 'open',

              titleAttr: 'Print',
              customize: function (doc){
                  $rootScope.customepdf(doc,'Nilai Pengetahuan','Rapor Digital Madrasah')
              }
          },
          {
              extend:    'excelHtml5',
              text:      '<i class="fa fa-file-text-o"></i> Excel',
              exportOptions: {
                  columns: [ 0,2,3,]
                },
              titleAttr: 'Excel'
          }
      ])
      .withOption('stateSave', true)
	
    .withOption('responsive', true)
    .withOption('lengthMenu', [[10, 25, 50, 100,200, -1], [10, 25, 50, 100,200, "All"]])
    .withOption('paging', false)	
    .withOption('processing', true);
    

}); 