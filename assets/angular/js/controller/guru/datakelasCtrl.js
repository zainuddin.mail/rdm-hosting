angular.module('RdmApp').controller('datakelasCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.tingkat = [];

	$scope.dtInstance = {};
	$scope.datakelasedit = {}; 
	$scope.saveeditkelas = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/kelas/save",$scope.datakelasedit,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getdatakelas(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {    
							$(".edit-modal").modal("show");
						}
					})
				}				
		});
	}
	$scope.editkelas = function(kelas_id,kelas_nama,tingkat_id,jurusan_id,guru_id,edit) {
		$scope.datakelasedit.kelas_id = kelas_id; 
		$scope.datakelasedit.kelas_nama = kelas_nama; 
		$scope.datakelasedit.tingkat_id = tingkat_id; 
		$scope.datakelasedit.jurusan_id = jurusan_id; 
		$scope.datakelasedit.guru_id = guru_id; 
		$scope.datakelasedit.edit = edit; 
			try{
			$scope.$apply();
			}catch(ex){
				
			}
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
	$scope.dtOptions = DTOptionsBuilder.newOptions()		
			.withOption('searching', true)
			.withOption('stateSave', true)
			.withButtons([
				  {
					  extend:    'copy',
					  text:      '<i class="fa fa-files-o"></i> Copy',
					  titleAttr: 'Copy'
				  },
				  {
					  extend:    'pdfHtml5',
					  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
					  download: 'open',
					  exportOptions: {
						  columns: [ 0,1,2,3,]
						},
					  titleAttr: 'Print',
					  customize: function (doc){
						  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
					  }
				  },
				  {
					  extend:    'excelHtml5',
					  text:      '<i class="fa fa-file-text-o"></i> Excel',
					  exportOptions: {
						  columns: [ 0,2,3,]
						},
					  titleAttr: 'Excel'
				  }
			  ])
			.withOption('paging', true)	
			.withOption('processing', true)
			.withPaginationType('full_numbers'); 

}); 