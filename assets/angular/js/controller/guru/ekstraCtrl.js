angular.module('RdmApp').controller('ekstraCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$scope.selectData = {};
	$scope.selectAll = "0";
	$scope.selectData.kelas_id = "";
	
	$rootScope.datasiswaextra ={};
    $scope.tingkatChange = function() {
		$scope.datasiswaextra = {}; 
		$scope.siswaextra = {}; 
		$scope.selectData.kelas_id = "";
	}
    $scope.chselectAll = function(selectAll) {
        angular.forEach($scope.siswaextra, function(siswa){
			siswa.select = selectAll;
        });
	}
    $scope.tambahpas = function() {
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");    
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
   
    }
	$rootScope.getdatatingkat = function() {
		ApiServer.get("guru/extra/datatingkat",function(response) {
				$rootScope.tingkat = response.tingkat; 
				$rootScope.jurusan = response.jurusan; 
				$rootScope.kelas = response.kelas; 
		});
	}
	$rootScope.getdatatingkat();
	$scope.datasiswaextra = [];
    $scope.tambahsiswapilihan = function() {
		$(".tambah-modal").modal("show");
	}
	$scope.savedatapilihan = function() {
		$(".tambah-modal").modal("hide");
		$rootScope.knilai= {};
		$rootScope.knilai.kirimnilai = [];
        $i = 0;
        $rootScope.knilai.extra = $rootScope.extra;
        $rootScope.knilai.kelas_id = $scope.selectData.kelas_id;
        angular.forEach($scope.siswaextra, function(siswa){
			if(siswa.select !=="0"){
				var datakirim = {};
				datakirim.siswa_id = siswa.siswa_id;
				$rootScope.knilai.kirimnilai[$i] = datakirim;
				$i++;
			}

        }); 
		$.blockUI({ });
		ApiServer.post("guru/extra/savepilihan",$rootScope.knilai,function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
					$scope.reloadTable(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
			}else if(response==null){
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: 'Data Gagal Disimpan'
					}).then((result) => {
						if (result.value) {   
							$(".tambah-modal").modal("show");
						}
					})
			}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {  
							$(".tambah-modal").modal("show");
						}
					})
			}			
		});
	}
	$scope.dodataDelete = function(id) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Ya, Hapus!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus siswa ini dari binaan anda?",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("guru/extra/deletepilihan",{siswa_id:id,ekstrakurikuler_id:$rootScope.extra,kelas_id:$scope.selectData.kelas_id},function(response) {
					//console.log(response);
						$.unblockUI({ });
						if(response.success){
							$scope.reloadTable(); 							
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})

	}

    $scope.simpanExtra = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.extra = [];
        angular.forEach($scope.datasiswaextra, function(siswa){
             var absen = {};
             absen.siswa_id = siswa.siswa_id;
             absen.kelas_id = $scope.selectData.kelas_id;
             absen.nilaiextra = siswa.nilaiextra;
            $rootScope.knilai.extra.push(absen);
        });
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		//console.log($rootScope.knilai);
		ApiServer.post("guru/extra/simpanextra/"+$rootScope.extra,$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.reloadTable(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {  
							$(".edit-modal").modal("show");
						}
					})
				}				
		});
	}
	$scope.siswaextra ={};
	$scope.reloadTable = function() {
		if($rootScope.extra!==''){
			$.blockUI({ });
			$scope.datasiswaextra = {};
			$scope.selectData.ekstrakurikuler_id = $rootScope.extra;
			ApiServer.post("guru/extra/datasiswa",$scope.selectData,function(response) {
					$.unblockUI({ });
					$scope.datasiswaextra = response.data; 
					$scope.siswaextra = response.siswa; 
					$rootScope.extranilailock = response.nilailock; 
			});
		}
	}


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withButtons([
          {
              extend:    'copy',
              text:      '<i class="fa fa-files-o"></i> Copy',
              titleAttr: 'Copy'
          },
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
              download: 'open',
              exportOptions: {
                  columns: [ 0,1,2,3,]
                },
              titleAttr: 'Print',
              customize: function (doc){
                  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
              }
          },
          {
              extend:    'excelHtml5',
              text:      '<i class="fa fa-file-text-o"></i> Excel',
              exportOptions: {
                  columns: [ 0,2,3,]
                },
              titleAttr: 'Excel'
          }
      ])
    .withOption('paging', false)	
    .withOption('responsive', true)
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);

    
    $scope.UploadTemplate = function() {
		if($scope.selectData.kelas_id ==""){
			Swal.fire({
				type: 'warning',
				title: 'Kelas Kosong',
				showConfirmButton: false,
				 html: "Pilih kelas terlebih dahulu",
				 timer: 1500,
			});
		}else{
			$rootScope.knilai ={};
			$rootScope.knilai.edit = false;
			$(".upload-modal").modal("show");
		}
        
    }
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'guru/extra/importnilai' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$scope.reloadTable(); 
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'guru/extra/importnilai/'+$rootScope.extra+"/"+$scope.selectData.kelas_id;;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};

}); 