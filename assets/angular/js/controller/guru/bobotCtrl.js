angular.module('RdmApp').controller('bobotCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];
	if($rootScope.datakelas[$rootScope.selectKelas] !== undefined){
		if($rootScope.datakelas[$rootScope.selectKelas].bahanajar !== undefined){
			$rootScope.bahanajar = $rootScope.datakelas[$rootScope.selectKelas].bahanajar;
		}else{
			$rootScope.datakelas[$rootScope.selectKelas].timeline = [];
		}
	}else{
		$rootScope.posts = [];
		$rootScope.datakelas[$rootScope.selectKelas] = [];
		$rootScope.datakelas[$rootScope.selectKelas].bahanajar = [];
	}
	$scope.dtInstance = {};
	$scope.currPg = 0;
	$scope.recordsDisplay = 0;
    $scope.savedataBobot = function() {
		$.blockUI({ });
		ApiServer.post("guru/kelas/savebobot/"+$rootScope.selectKelas,$rootScope.bobot,function(response) {
			$.unblockUI({ });
			if(response!==null && response.success){
					$rootScope.getbobotkelas();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
			}else if(response==null){
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: 'Data Gagal Disimpan'
					})
			}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
			}			
		});
	}

	$scope.dtOptions = DTOptionsBuilder.newOptions()		
	.withOption('searching', true)
	.withOption('stateSave', true)
	.withButtons([
		  {
			  extend:    'copy',
			  text:      '<i class="fa fa-files-o"></i> Copy',
			  titleAttr: 'Copy'
		  },
		  {
			  extend:    'pdfHtml5',
			  text:      '<i class="fa fa-print" aria-hidden="true"></i> Print',
			  download: 'open',
			  exportOptions: {
				  columns: [ 0,1,2,3,]
				},
			  titleAttr: 'Print',
			  customize: function (doc){
				  $rootScope.customepdf(doc,'Absen Siswa','Rapor Digital Madrasah')
			  }
		  },
		  {
			  extend:    'excelHtml5',
			  text:      '<i class="fa fa-file-text-o"></i> Excel',
			  exportOptions: {
				  columns: [ 0,2,3,]
				},
			  titleAttr: 'Excel'
		  }
	  ])
	.withOption('paging', true)	
	.withOption('processing', true)
	.withOption('AutoWidth', true)
	.withOption('responsive', true)
	.withOption('columnDefs',[
		{ "width": "2%", "targets": 0 },
		{ "width": "10%", "targets": -2 },
		{ "width": "16%", "targets": -1 },
		{ responsivePriority: 1, targets: 2 },
		{ responsivePriority: 2, targets: -1 }
	])
	.withOption('drawCallback', function(settings) {
		if(settings.aoData.length > 0) {
			var api = this.api();
			var pgNo = api.page.info();
			$scope.currPg = pgNo.page;
			$scope.recordsDisplay =	pgNo.recordsDisplay;
		   }
		$('.lazy').Lazy();
		try{
			$scope.$apply();
			}catch(ex){
				
			}
	})
	.withPaginationType('full_numbers'); 
}); 