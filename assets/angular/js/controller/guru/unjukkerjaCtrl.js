angular.module('RdmApp').controller('unjukkerjaCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer, FileUploader) {
	$rootScope.knilai ={};
	$scope.setallNilai = function(allNilai) {
		if(allNilai !==null && allNilai !==""){
			angular.forEach($rootScope.dataharian, function(siswa){
				if($rootScope.knilai.tambahharian == undefined){
					$rootScope.knilai.tambahharian = {};
				}
				$rootScope.knilai.tambahharian[siswa.siswa_id] = allNilai;
			});
		}
	}
    $scope.editKompnilai = function(edit) {
        $rootScope.knilai.edit = true;
        $rootScope.knilai.komponennilai_id = edit.komponennilai_id;
        $rootScope.knilai.komponennilai_materi = edit.komponennilai_materi;
        $rootScope.knilai.komponennilai_nama = edit.komponennilai_nama;
        $rootScope.knilai.tambahharian = {};
        angular.forEach($rootScope.dataketerampilan, function(siswa){
			if(siswa.nilaiuk!== null && siswa.nilaiuk[edit.komponennilai_id] !== undefined){
            	$rootScope.knilai.tambahharian[siswa.siswa_id] = parseInt(siswa.nilaiuk[edit.komponennilai_id]) ;
			}else{
				$rootScope.knilai.tambahharian[siswa.siswa_id] = 0;
			}
        });
        $(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });
      
    }
    $scope.tambahharian = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

        
    }
    $scope.deleteKompnilai = function(data) {
		Swal.fire({
			type: 'warning',
			title: 'Perhatian',
			confirmButtonText: 'Ya, Hapus!',
			showCancelButton: true,
			html: "Apakah Anda yakin akan menghapus nilai ini? Nilai yang sudah di hapus tidak bisa dikembalikan lagi.",
		}).then((result) => {
			//console.log(result);
			if (result.value) {   
				$.blockUI({ });
				ApiServer.post("guru/keterampilan/deletenilai",data,function(response) {
					//console.log(response);
						$.unblockUI({ });
                        $rootScope.getketerampilan(); 
						if(response.success){
							Swal.fire({
								type: 'success',
								title: 'Berhasil',
								showConfirmButton: false,
								 html: 'Data Berhasil Dihapus',
								 timer: 1000,
							})
						}else{
							Swal.fire({
								type: 'error',
								title: 'Gagal',
								showConfirmButton: true,
								 html: response.message
							})
						}				
				});
			}
		})


    }
    $scope.savedataharian = function() {
        //console.log($rootScope.knilai);

		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/keterampilan/saveunjukkerja",$rootScope.knilai,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getketerampilan(); 
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							  });
					
						}
					})
				}				
		});
	}
	


    $scope.dtOptions = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
	.withOption('stateSave', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);
    $scope.dtOptions2 = DTOptionsBuilder.newOptions()		
    .withOption('searching', true)
    .withOption('paging', false)	
    .withOption('ordering', false)	
    .withOption('processing', false);

	$scope.UploadTemplate = function() {
		$rootScope.knilai ={};
        $rootScope.knilai.edit = false;
        $(".upload-modal").modal("show");
        
    }
	var uploader = $scope.uploader = new FileUploader({
		url: base_url + 'guru/keterampilan/importnilai' 
	});
	

	$scope.berhasilaa = 0;
	$scope.gandaaa = 0;
	$scope.gagalaa = 0;

	// FILTERS
	$scope.hapussemua = function() {
		uploader.clearQueue();
		$scope.berhasilaa = 0;
		$scope.gandaaa = 0;
		$scope.gagalaa = 0;

	}

	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 100;
		}
	});
	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.name.substr(item.name.lastIndexOf('.')+1) + '|';
			console.info('type', type);
			return '|xls|xlsx|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.log(response);
		$scope.berhasilka = $scope.berhasilka + response.sukses;
		$scope.gandaaa = $scope.gandaaa + response.double1;
		$scope.gagalaa = $scope.gagalaa + response.gagal;
		
		$scope.$apply();
		console.info('onSuccessItem', fileItem, response, status, headers, $scope.gandaaa);
	};
	uploader.onCompleteAll = function() {
		$rootScope.getketerampilan(); 
	};
	uploader.onBeforeUploadItem = function(item) {
		item.url = base_url + 'guru/keterampilan/importnilai/praktek/'+$rootScope.selectKelas;
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		uploader.uploadAll();
	};

}); 