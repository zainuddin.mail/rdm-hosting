angular.module('RdmApp').controller('kelasCtrl', function ($cookies,$scope,$rootScope,$stateParams,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	$rootScope.dataharian = [];
	$rootScope.dataketerampilan = [];
	$rootScope.bobot = [];
	$rootScope.kkmdata = [];
	$rootScope.selectKelas = $stateParams.idkelas;
	$cookies.put('selectkelas',$rootScope.selectKelas);
	//////console.log($rootScope.selectKelas);
	$rootScope.getbobotkelas = function() {
		ApiServer.get("guru/kelas/bobot",function(response) {
			$rootScope.kkmdata = response.data; 
			$rootScope.bobot = response.bobot; 
			if($rootScope.kkmdata.length == 0){
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
					 html: "KKM Tingkat belum di isi oleh admin madrasah."
				})
			}else{
				$rootScope.getharian();	
				$rootScope.getketerampilan();	
				//$rootScope.updateSikap();	
			}
		});
	}

	$rootScope.getbobotkelas();	
	$rootScope.updateSikap =function() {
		if($rootScope.datakelasSelect.mapel_jenisnilai !=="0" && $rootScope.datakelasSelect.mapel_jenisnilai !== undefined){
			$scope.getSikap($rootScope.datakelasSelect.mapel_jenisnilai);
		}
	}
	$scope.getSikap =function(idsikap) {
		$.blockUI({ });
		ApiServer.get("guru/sikap/datasikap/"+idsikap,function(response) {
				$.unblockUI({ });
				$rootScope.datasikap = []; 
				$rootScope.datakomponensikap = response.komponen; 
				$rootScope.datanilaisikap = response.datanilai; 
				$rootScope.jenisnilai = response.jenisnilai; 
                angular.forEach($rootScope.datasiswa, function(siswa){
					var sispils = false;
					if($rootScope.datakelasSelect.mapel_pilihan =="1"){
						$scope.FilPilihan = $rootScope.siswapilihan.filter(function (data) {
							return (data.siswa_id == siswa.siswa_id);
						});
						if($scope.FilPilihan.length > 0){
							sispils = true;
						}
					}
					if($rootScope.datakelasSelect.mapel_pilihan =="0" || sispils == true){
						$rootScope.datasikap.push(siswa);
					}
				});
				$rootScope.jnilai = 0;
                angular.forEach($rootScope.jenisnilai, function(jenis){
					////console.log("Jenis length:"+jenis.cnilai);
					if(jenis.cnilai > 0 ) $rootScope.jnilai++;
				});
				//////console.log("JJenis = "+$rootScope.jnilai);
		});
	}

	$scope.filterKelas = function(){
		$rootScope.datakelasSelect = [];
		$scope.filterdata = $rootScope.datakelas.filter(function (kelas) {
			return (kelas.ajar_id == $rootScope.selectKelas);
		});
		if($scope.filterdata.length > 0){
			$rootScope.datakelasSelect = $scope.filterdata[0];
		}
		//////console.log($rootScope.datakelasSelect.mapel_jenisnilai);
		
	}
	$scope.filterKelas();
	$scope.$on("KelasChange", function(evt,data){ 
		//////console.log(data);
        if(data.length > 0){
            $scope.filterKelas();
        }
    });
	$scope.getdatasiswa = function() {
		ApiServer.get("guru/kelas/datasiswa",function(response) {
				$rootScope.datasiswa = response.data; 
		});
	}
	$scope.getdatasiswa();	
	$rootScope.dataRapor = {};	
	$rootScope.dataRapork = {};	
    $rootScope.getharian = function() {
		$.blockUI({ });
		ApiServer.get("guru/pengetahuan/harian",function(response) {
				$.unblockUI({ });
				$rootScope.dataharian = []; 
				$rootScope.datapilihan = []; 
				$rootScope.datakomponen = response.komponen; 
				$rootScope.datanilai = response.datanilai; 
				$rootScope.datapas = response.datapas; 
				$rootScope.dataRapor = response.datarapor; 
				$rootScope.siswapilihan = response.siswapilihan; 
				$rootScope.nilailock = response.nilailock; 
				//////console.log("$rootScope.dataRapor");	
				//////console.log($rootScope.dataRapor);	
                angular.forEach($rootScope.datasiswa, function(siswa){
					var sispil = false;
					if($rootScope.datakelasSelect.mapel_pilihan =="1"){
						$scope.FilPilihan = $rootScope.siswapilihan.filter(function (data) {
							return (data.siswa_id == siswa.siswa_id);
						});
						if($scope.FilPilihan.length > 0){
							sispil = true;
						}
					}
					if($rootScope.datakelasSelect.mapel_pilihan =="0" || sispil == true){
						var nilaimax = 0;
						var descmax = "";
						var descmin = "";
						var nilaimin = 100;
						console.log("Paspat");
						if($rootScope.datapas[siswa.siswa_id] !== undefined){
							siswa.paspat = parseInt($rootScope.datapas[siswa.siswa_id]);
						}else{
							siswa.paspat = "";
							if($rootScope.bobot.bobotkelas['paspat'] > 0){
								nilaimin = 0;
							}
						}
						console.log(siswa.paspat);
						if($rootScope.datanilai[siswa.siswa_id] !== undefined){
							siswa.nilai = $rootScope.datanilai[siswa.siswa_id];

							var nilaiharian = 0;
							angular.forEach($rootScope.datakomponen, function(komp){
								var nilaikomp;
								if(siswa.nilai[komp.komponennilai_id] !== undefined){
									nilaikomp = parseInt(siswa.nilai[komp.komponennilai_id]);
									
								}else{
									nilaikomp = 0;
								}
								if(nilaikomp > nilaimax){
									nilaimax = nilaikomp;
									descmax = komp.komponennilai_materi;
								}
								if(nilaikomp < nilaimin){
									nilaimin = nilaikomp;
									descmin = komp.komponennilai_materi;
								}
								nilaiharian +=nilaikomp;

							});
							siswa.nilaiharian = Math.round((nilaiharian/$rootScope.datakomponen.length),0);
						}else{
							siswa.nilai = null;
							siswa.nilaiharian = 0;
							if($rootScope.bobot.bobotkelas['harian'] > 0){
								nilaimin = 0;
							} 
							if($rootScope.bobot.bobotkelas['paspat'] > 0){
								nilaimin = 0;
							}
						}
						if($rootScope.dataRapor[siswa.siswa_id] !== undefined){
							siswa.rapor = $rootScope.dataRapor[siswa.siswa_id].rapor_nilai;
							siswa.predikat = $rootScope.dataRapor[siswa.siswa_id].rapor_predikat;
							siswa.desc = $rootScope.dataRapor[siswa.siswa_id].rapor_deskripsi;
						}else{
							if(siswa.nilaiharian + siswa.paspat > 0){
								siswa.rapor = Math.round((((siswa.nilaiharian * $rootScope.bobot.bobotkelas['harian']) + (siswa.paspat * $rootScope.bobot.bobotkelas['paspat']))/( $rootScope.bobot.bobotkelas['harian'] + $rootScope.bobot.bobotkelas['paspat'])),2);
							}else{
								siswa.rapor = 0;
							}
							$scope.FilPredikat = $rootScope.kkmdata.kkmgrade.filter(function (data) {
								return (parseInt(data.kkmgrade_min) <= siswa.rapor && parseInt(data.kkmgrade_max) >= siswa.rapor);
							});
							////console.log($scope.FilPredikat);
							if($scope.FilPredikat.length > 0){
								siswa.predikat = $scope.FilPredikat[0].kkmgrade_kode;
								var kkm = parseInt($rootScope.kkmdata.kkmtingkat_nilai);
								var desc = "";
								if(siswa.predikat == "A"){
									desc = "sangat baik";
		
								}else if(siswa.predikat == "B"){
									desc = "baik";
								}else if(siswa.predikat == "C"){
									desc = "cukup baik";
								}else{
									desc = "belum optimal";
								} 
								if( kkm <= nilaimin && kkm <= nilaimax && kkm <= siswa.rapor){
									//siswa.desc ="Memiliki kompetensi semua materi dengan " + desc + " terutama menguasai "+ descmax;
									siswa.desc ="Memiliki kemampuan " + desc + " terutama kemampuan dalam "+ descmax;
								}else if(kkm <= nilaimin && kkm <= nilaimax && kkm > siswa.rapor){
									//siswa.desc ="Memiliki kompetensi semua materi dengan " + desc + ". Menguasai "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
									if(nilaimin == nilaimax){
										siswa.desc ="Memiliki kemampuan " + desc + " pada seluruh materi terutama dalam "+ descmax;
									}else{
										siswa.desc ="Memiliki kemampuan " + desc + ". Memiliki kemampuan baik dalam "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
									}
								}else if(kkm > nilaimin && nilaimin > 0 && kkm <= nilaimax){
									//siswa.desc ="Memiliki kompetensi semua materi dengan " + desc + ". Menguasai "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
									if(nilaimin == nilaimax){
										siswa.desc ="Memiliki kemampuan " + desc + " pada seluruh materi terutama dalam "+ descmin;
									}else{
										siswa.desc ="Memiliki kemampuan " + desc + ". Memiliki kemampuan baik dalam "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
									}
								}else{
									//siswa.desc ="Perlu pebaikan kompetensi pada seluruh materi";
									siswa.desc ="Memiliki kemampuan belum optimal pada seluruh materi";
								}
								if(siswa.rapor == 0){
									siswa.desc = "";
								}
							}else{
								siswa.predikat = "-";
								siswa.desc = "";
							}
						}

						console.log("Harian Siswa");
						console.log(siswa);
						$rootScope.dataharian.push(siswa);
					}else{
						siswa.select = "0";
						$rootScope.datapilihan.push(siswa);
						////console.log($rootScope.datakelasSelect.mapel_pilihan);
						//////console.log("Mapel Pilihan");
					}
                })
		});
	}
	$scope.nilaikd = [];
    $rootScope.getketerampilan = function() {
		ApiServer.get("guru/keterampilan/dataketerampilan",function(response) {
				$rootScope.dataketerampilan = []; 
				$rootScope.komponenuk = response.komponenuk; 
				$rootScope.datauk = response.datauk; 
				$rootScope.komponenpro = response.komponenpro; 
				$rootScope.datapro = response.datapro; 
				$rootScope.komponenpor = response.komponenpor; 
				$rootScope.datapor = response.datapor; 
				$rootScope.dataRapork = response.datarapork; 
				$rootScope.siswapilihank = response.siswapilihan;
                angular.forEach($rootScope.datasiswa, function(siswa){
					var sispilk = false;
					$scope.nilaikd = [];
					$scope.finalkd = [];
					if($rootScope.datakelasSelect.mapel_pilihan =="1"){
						$scope.FilPilihank = $rootScope.siswapilihank.filter(function (data) {
							return (data.siswa_id == siswa.siswa_id);
						});
						if($scope.FilPilihank.length > 0){
							sispilk = true;
						}
					}
					if($rootScope.datakelasSelect.mapel_pilihan =="0" || sispilk == true){
						var nilaimax = 0;
						//var descmax = "";
						//var descmin = "";
						var nilaimin = 100;
						siswa.uknilai = [];
						if($rootScope.datauk[siswa.siswa_id] !== undefined){
							siswa.nilaiuk = $rootScope.datauk[siswa.siswa_id];
							var nilaiuk = 0;
							angular.forEach($rootScope.komponenuk, function(komp){
								var nilaikomp;
								if(siswa.nilaiuk[komp.komponennilai_id] !== undefined){
									nilaikomp = parseInt(siswa.nilaiuk[komp.komponennilai_id]);
									siswa.uknilai[komp.komponennilai_nama] = nilaikomp;
								}else{
									nilaikomp = 0;
									siswa.uknilai[komp.komponennilai_nama] = 0;
								}
								if($rootScope.bobot.bobotkelas['praktek'] > 0){
									if(nilaikomp > nilaimax){
										nilaimax = nilaikomp;
									}
									if(nilaikomp < nilaimin){
										nilaimin = nilaikomp;
									}
								}
								if($scope.nilaikd[komp.komponennilai_nama] == undefined){
									$scope.nilaikd[komp.komponennilai_nama] = {}
									$scope.nilaikd[komp.komponennilai_nama].nilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].tnilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].uk = 0;
									$scope.nilaikd[komp.komponennilai_nama].pro = 0;
									$scope.nilaikd[komp.komponennilai_nama].por = 0;
								}
								$scope.nilaikd[komp.komponennilai_nama].komponennilai_materi = komp.komponennilai_materi;
								$scope.nilaikd[komp.komponennilai_nama].nama = komp.komponennilai_nama;
								$scope.nilaikd[komp.komponennilai_nama].tnilai++;
								$scope.nilaikd[komp.komponennilai_nama].nilai += nilaikomp;
								$scope.nilaikd[komp.komponennilai_nama].uk = nilaikomp;
								nilaiuk +=nilaikomp;
							});
							siswa.uk = Math.round((nilaiuk/$rootScope.komponenuk.length),0);
						}else{
							siswa.nilaiuk = null;
							siswa.uk = 0;
							if($rootScope.bobot.bobotkelas['praktek'] > 0){
								nilaimin = 0;
							}
						}
						siswa.pronilai = [];
						if($rootScope.datapro[siswa.siswa_id] !== undefined){
							siswa.nilaipro = $rootScope.datapro[siswa.siswa_id];
							var nilaipro = 0;
							angular.forEach($rootScope.komponenpro, function(komp){
								var nilaikomp;
								if(siswa.nilaipro[komp.komponennilai_id] !== undefined){
									nilaikomp = parseInt(siswa.nilaipro[komp.komponennilai_id]);
									siswa.pronilai[komp.komponennilai_nama] = nilaikomp;
								}else{
									siswa.pronilai[komp.komponennilai_nama] = 0;
									nilaikomp = 0;
								}
								if($rootScope.bobot.bobotkelas['proyek'] > 0){
									if(nilaikomp > nilaimax){
										nilaimax = nilaikomp;
										descmax = komp.komponennilai_materi;
									}
									if(nilaikomp < nilaimin){
										nilaimin = nilaikomp;
										descmin = komp.komponennilai_materi;
									}
								}
								if($scope.nilaikd[komp.komponennilai_nama] == undefined){
									$scope.nilaikd[komp.komponennilai_nama] = {}
									$scope.nilaikd[komp.komponennilai_nama].nilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].tnilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].uk = 0;
									$scope.nilaikd[komp.komponennilai_nama].pro = 0;
									$scope.nilaikd[komp.komponennilai_nama].por = 0;
								}
								$scope.nilaikd[komp.komponennilai_nama].komponennilai_materi = komp.komponennilai_materi;
								$scope.nilaikd[komp.komponennilai_nama].nama = komp.komponennilai_nama;
								$scope.nilaikd[komp.komponennilai_nama].nilai += nilaikomp;
								$scope.nilaikd[komp.komponennilai_nama].pro = nilaikomp;
								$scope.nilaikd[komp.komponennilai_nama].tnilai++;
								nilaipro +=nilaikomp;
							});
							siswa.pro = Math.round((nilaipro/$rootScope.komponenpro.length),0);
						}else{
							siswa.nilaipro = null;
							siswa.pro = 0;
							if($rootScope.bobot.bobotkelas['proyek'] > 0){
								nilaimin = 0;
							} 
						}
						siswa.pornilai = [];
						if($rootScope.datapor[siswa.siswa_id] !== undefined){
							siswa.nilaipor = $rootScope.datapor[siswa.siswa_id];
							var nilaipor = 0;
							angular.forEach($rootScope.komponenpor, function(komp){
								var nilaikomp;
								if(siswa.nilaipor[komp.komponennilai_id] !== undefined){
									nilaikomp = parseInt(siswa.nilaipor[komp.komponennilai_id]);
									siswa.pornilai[komp.komponennilai_nama] = nilaikomp;
								}else{
									siswa.pornilai[komp.komponennilai_nama] = 0;
									nilaikomp = 0;
								}
								if($rootScope.bobot.bobotkelas['porto'] > 0){
									if(nilaikomp > nilaimax){
										nilaimax = nilaikomp;
										descmax = komp.komponennilai_materi;
									}
									if(nilaikomp < nilaimin){
										nilaimin = nilaikomp;
										descmin = komp.komponennilai_materi;
									}
								}
								if($scope.nilaikd[komp.komponennilai_nama] == undefined){
									$scope.nilaikd[komp.komponennilai_nama] = {}
									$scope.nilaikd[komp.komponennilai_nama].nilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].tnilai = 0;
									$scope.nilaikd[komp.komponennilai_nama].uk = 0;
									$scope.nilaikd[komp.komponennilai_nama].pro = 0;
									$scope.nilaikd[komp.komponennilai_nama].por = 0;
								}
								$scope.nilaikd[komp.komponennilai_nama].komponennilai_materi = komp.komponennilai_materi;
								$scope.nilaikd[komp.komponennilai_nama].nama = komp.komponennilai_nama;
								$scope.nilaikd[komp.komponennilai_nama].nilai += nilaikomp;
								$scope.nilaikd[komp.komponennilai_nama].por = nilaikomp;
								$scope.nilaikd[komp.komponennilai_nama].tnilai++;
								nilaipor +=nilaikomp;

							});
							siswa.por = Math.round((nilaipor/$rootScope.komponenpor.length),0);
						}else{
							siswa.nilaipor = null;
							siswa.por = 0;
							if($rootScope.bobot.bobotkelas['porto'] > 0){
								nilaimin = 0;
							} 
						}
						console.log($scope.nilaikd);

							siswa.finalkd =[];
							if($scope.nilaikd.length > 0){
								var nilaiku = 0
								var tnilaiku = 0;
								var dekskdmax = "";
								var deskminkd = "";
								var kdmin  = 100;
								var kdmax = 0;
								angular.forEach($scope.nilaikd, function(nilai){
									tnilaiku++;
									if(nilai.tnilai > 0){
										nilai.nilaikd = (nilai.nilai/nilai.tnilai);
										nilaiku += (nilai.nilai/nilai.tnilai) ;
										if(kdmax < nilai.nilaikd){
											dekskdmax = nilai.komponennilai_materi;
											kdmax = nilai.nilaikd;
										}
										if(kdmin > nilai.nilaikd){
											deskminkd = nilai.komponennilai_materi;
											kdmin = nilai.nilaikd;
										}
									}
									$scope.finalkd.push(nilai);
									siswa.finalkd.push(nilai);
								})
								console.log($scope.finalkd);
								console.log(siswa.finalkd);
								siswa.rapork = Math.round((nilaiku/tnilaiku),2);
							}else{
								siswa.rapork = 0;
							}
							$scope.FilPredikat = $rootScope.kkmdata.kkmgrade.filter(function (data) {
								return (parseInt(data.kkmgrade_min) <= siswa.rapork && parseInt(data.kkmgrade_max) >= siswa.rapork);
							});
							//////console.log($scope.FilPredikat);
							if($scope.FilPredikat.length > 0){
								siswa.predikatk = $scope.FilPredikat[0].kkmgrade_kode;
								var kkm = parseInt($rootScope.kkmdata.kkmtingkat_nilai);
								var desc = "";
								if(siswa.predikatk == "A"){
									desc = "Sangat terampil terutama dalam "+ dekskdmax;;

								}else if(siswa.predikatk == "B"){
									desc = "Terampil terutama dalam "+ dekskdmax;
								}else if(siswa.predikatk == "C"){
									desc = "Cukup terampil terutama dalam "+ dekskdmax;
								}else{
									if(kdmin > 0){
										desc = "Kurang terampil terutama dalam "+ deskminkd;
									}else{
										desc = "Kurang terampil terutama dalam "+ dekskdmax;
									}
								}
								//if(kkm <= nilaimax){
									siswa.desck = desc ;
								//}else if(kkm > nilaimin && nilaimin > 0 && kkm <= nilaimax){
								//	siswa.desck ="Memiliki keterampilan semua materi dengan " + desc + " terutama menguasai "+ descmax +", dan perlu ditingkatkan dalam " + descmin;
								//}else{
								//	siswa.desck ="Perlu pebaikan keterampilan pada seluruh materi";
								//}
								if(siswa.rapork == 0){
									siswa.desck = "";
								}
							}else{
								siswa.predikatk = "-";
							}
						if($rootScope.dataRapork[siswa.siswa_id] !== undefined){
							siswa.rapork = $rootScope.dataRapork[siswa.siswa_id].rapor_nilai;
							siswa.predikatk = $rootScope.dataRapork[siswa.siswa_id].rapor_predikat;
							siswa.desck = $rootScope.dataRapork[siswa.siswa_id].rapor_deskripsi;
						}
						console.log(siswa);
						$rootScope.dataketerampilan.push(siswa);
						console.log($scope.dtInstance);
						//$scope.dtInstance.DataTable().destroy();
					}
                })
		});
	}


}); 