angular.module('RdmApp').controller('DashboardCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer) {
	//$rootScope.dashboard = [];
	$scope.editProfile = function() {
		$(".edit-modal").modal("show");
		$('.modal-dialog').draggable({
			handle: ".modal-header"
		  });

	}
	$scope.getdashboard = function() {
		ApiServer.get("guru/dashboard",function(response) {
				$rootScope.dashboard = response.data; 
		});
	}
	$scope.savedataEdit = function() {
		$(".edit-modal").modal("hide");
		$.blockUI({ });
		ApiServer.post("guru/saveprofile",$rootScope.profile,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$rootScope.getprofile();
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					}).then((result) => {
						if (result.value) {   
							$(".edit-modal").modal("show");
						}
					})
				}				
		});
	}

	$scope.getdashboard();	
	$scope.browseimage = function(){
		$("#imageFile").click();
	}	
	$scope.$on("pictureInsert", function(evt,data){ 
		$rootScope.profile.guru_foto =data;	
	});
}); 


angular.module('RdmApp').directive('myDirective', function (httpPostFactory) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
				$.blockUI({ });
                var formData = new FormData();
                formData.append('file', element[0].files[0]);
                var url = element[0].getAttribute("data-url");
                var container = element[0].getAttribute("image-container");
                httpPostFactory(url, formData, function (callback) {
				   // recieve image name to use in a ng-src 
				   $.unblockUI({ });
				   if(callback && callback.success){
						scope.$emit('pictureInsert',callback.data);	
											
				   }else{
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: callback.data
						})
				   }
				   
                });
            });

        }
    };
});