angular.module('RdmApp').controller('timelineCtrl', function ($scope,$rootScope,common,DTOptionsBuilder, DTColumnBuilder, $http, $q, ApiServer,nowTime) {
	//$rootScope.dashboard = [];
	// Variables
	$rootScope.dataTimeline = [];
	if($rootScope.datakelas[$rootScope.selectKelas] !== undefined){
		//console.log($rootScope.datakelas[$rootScope.selectKelas]);
		if($rootScope.datakelas[$rootScope.selectKelas].timeline !== undefined){
			$rootScope.dataTimeline = $rootScope.datakelas[$rootScope.selectKelas].timeline;
		}else{
			$rootScope.datakelas[$rootScope.selectKelas].timeline = [];
		}
	}else{
		$rootScope.datakelas[$rootScope.selectKelas] = [];
		$rootScope.datakelas[$rootScope.selectKelas].timeline = [];
	}
	$scope.nowTime = nowTime;
    $scope.row = 0;
    $scope.rowperpage = 3;
	$scope.newPost = {};
	$scope.newKomen = {};
	$scope.newPost.image = "";
	$scope.newPost.link = "";
	$scope.newPost.text = "";
	$scope.newPost.type = 3;
    $scope.busy = false;
    $scope.loading = false;       
    // Fetch data
    $scope.setType = function(type){
		$scope.newPost.type = type;
	}
    $scope.saveComent = function(data,comment){
		$.blockUI({ });
		$scope.newKomen = {};
		$scope.newKomen.forumrespon_komentar = comment;
		$scope.newKomen.forum_id = data.forum_id;
		ApiServer.post("guru/timeline/savekomen",$scope.newKomen,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.updatePosts(data);
					$scope.newPost.image = "";
					$scope.newPost.link = "";
					$scope.newPost.text = "";
					$scope.newPost.type = 3;
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
    $scope.browseimage = function(){
		$("#imageFile").click();
	}
    $scope.removeImage = function(){
		$scope.newPost.image ="";
	}
	$scope.sendPost = function() {
		$.blockUI({ });
		ApiServer.post("guru/timeline/save",$scope.newPost,function(response) {
			//console.log(response);
				$.unblockUI({ });
				if(response.success){
					$scope.getnewPosts();
					$scope.newPost.image = "";
					$scope.newPost.link = "";
					$scope.newPost.text = "";
					$scope.newPost.type = 1;
					Swal.fire({
						type: 'success',
						title: 'Berhasil',
						showConfirmButton: false,
						 html: 'Data Berhasil Disimpan',
						 timer: 1000,
					})
				}else{
					Swal.fire({
						type: 'error',
						title: 'Gagal',
						showConfirmButton: true,
						 html: response.message
					})
				}				
		});
	}
    $scope.getnewPosts = function(){
		if($rootScope.dataTimeline.length > 0){
			forum_id = $rootScope.dataTimeline[0].forum_id;
		}
		ApiServer.get("guru/timeline/newdata/"+forum_id,function(response) {
			if(response.success){
				$scope.row+=$scope.rowperpage;
				$scope.$apply(function(){      
					angular.forEach(response.data,function(item) {
						$rootScope.dataTimeline.unshift(item);
					});
					if($rootScope.datakelas[$rootScope.selectKelas] == undefined){
						$rootScope.datakelas[$rootScope.selectKelas] = [];
					}
					if($rootScope.datakelas[$rootScope.selectKelas].timeline == undefined){
						$rootScope.datakelas[$rootScope.selectKelas].timeline = [];		
					}	
					$rootScope.datakelas[$rootScope.selectKelas].timeline = $rootScope.dataTimeline;
					setTimeout(function() {
						$('.lazy').Lazy();			
					},1000);					
				});
				
			}
		});
	}
    $scope.updatePosts = function(dt){
		$.blockUI({ });
		forum_id = dt.forum_id;
		ApiServer.get("guru/timeline/getupdatedata/"+forum_id,function(response) {
			$.unblockUI({ });
			if(response.success){
				dt = response.data;
				const indexOfItemInArray = $rootScope.dataTimeline.findIndex(q => q.forum_id === response.data.forum_id);
				$rootScope.dataTimeline[indexOfItemInArray] = response.data;
				if($rootScope.datakelas[$rootScope.selectKelas] == undefined){
					$rootScope.datakelas[$rootScope.selectKelas] = [];
				}
				if($rootScope.datakelas[$rootScope.selectKelas].timeline == undefined){
					$rootScope.datakelas[$rootScope.selectKelas].timeline = [];		
				}	
				$rootScope.datakelas[$rootScope.selectKelas].timeline = $rootScope.dataTimeline;
		}
		});
	}
    $scope.getPosts = function(){
		if ($scope.busy) return;
		$scope.loading = true;
		$scope.busy = true;
		forum_id ="0";
		if($rootScope.dataTimeline.length > 0){
			forum_id = $rootScope.dataTimeline[$rootScope.dataTimeline.length-1].forum_id;
		}
		ApiServer.get("guru/timeline/data/"+forum_id,function(response) {
			if(response.success){
				if(response.data.length > 0){
					$scope.row+=$scope.rowperpage;
					$scope.$apply(function(){   
						//console.log($rootScope.dataTimeline);   
						angular.forEach(response.data,function(item) {
							$rootScope.dataTimeline.push(item);
						});
						if($rootScope.datakelas[$rootScope.selectKelas] == undefined){
							$rootScope.datakelas[$rootScope.selectKelas] = [];
						}
						if($rootScope.datakelas[$rootScope.selectKelas].timeline == undefined){
							$rootScope.datakelas[$rootScope.selectKelas].timeline = [];		
						}				
						$rootScope.datakelas[$rootScope.selectKelas].timeline = $rootScope.dataTimeline;
						$scope.busy = false;
						$scope.loading = false;
						setTimeout(function() {
							$('.lazy').Lazy();			
						},1000);					
					});
				}else{
					$scope.busy = true;
					$scope.loading = false;
				}

				
			}else{
				$scope.busy = false;
				$scope.loading = false;
			}
		});

	}
    $scope.$on("pictureInsert", function(evt,data){ 
		$scope.newPost.image =data;
		//console.log($scope.newPost.image);
	});
	if($rootScope.dataTimeline.length == 0){
		$scope.getPosts();
	}
	$("#postText").focus();
}); 
angular.module('RdmApp').directive('myDirective', function (httpPostFactory) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
				$.blockUI({ });
                var formData = new FormData();
                formData.append('file', element[0].files[0]);
                var url = element[0].getAttribute("data-url");
                var container = element[0].getAttribute("image-container");
                httpPostFactory(url, formData, function (callback) {
				   // recieve image name to use in a ng-src 
				   $.unblockUI({ });
				   if(callback && callback.success){
						scope.$emit('pictureInsert',callback.data);						
				   }else{
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: callback.data
						})
				   }
				   
                });
            });

        }
    };
});