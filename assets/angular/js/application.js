var d = new Date();
var versionApp = "?"+RDM_version;
var TimeZoneDiv = TzDifference-d.getTimezoneOffset();
var RdmApp = angular.module('RdmApp', [
	"ngCookies",
	"ui.router",
	"ui.bootstrap",
	"oc.lazyLoad",
	"ngSanitize",
	"datatables",
	'angularFileUpload',
	'infinite-scroll',
	'ADM-dateTimePicker',
	"datatables.buttons"
	]);

// Constant Here
RdmApp.constant('ConstantAlias', {});
RdmApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
 
}]);
RdmApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        
    });
}]);
RdmApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);
RdmApp.run(["$rootScope", '$location',"$state",'$http','$window','$cookies', function($rootScope, $location, $state,$http,$window,$cookies) {
     // state to be accessed from view
	$rootScope.$state = $state;
	$rootScope.userData = $cookies.get('rdmData') || {};
	$rootScope.$on('$stateChangeStart', function() {
		jQuery.blockUI({ });
	});
	$rootScope.$on('$viewContentLoaded', function() {
		try {
			jQuery.unblockUI({ });
			//console.clear();
		}
		catch(err) {

		}
	});
	$rootScope.$on('$stateChangeSuccess', function() {
		jQuery.blockUI({ });
		setTimeout(function(){
			//handleSidebarMenuActiveLink('match', null, $state);
		}, 500);
	});

	$state.defaultErrorHandler(function(error) {
		// This is a naive example of how to silence the default error handler.
		window.myAppErrorLog.push(error);
	  });
	$rootScope.$on('$stateChangeError', function() {
		jQuery.blockUI({ });
		Swal.fire({
			type: 'error',
			title: 'Error',
			showConfirmButton: true,
			 html: "Tidak dapat memuat halaman yang anda minta."
		})
	});
	$rootScope.$on('$stateNotFound', function() {
		 jQuery.unblockUI({ });
	});
	$rootScope.$on('$locationChangeStart', function (event, next, current) {
		setTimeout(function(){
			$(".datepicker").hide();
		}, 500);
		var userData = $cookies.get('rdmData') || "";
		var revision = $cookies.get('_revision') || "";
		if(userData == ""){
			//console.log("Not Login");
			window.location.reload();
		}else{
			if(revision !== RDM_version){
				//console.log("Versi berbeda "+revision+"-"+RDM_version);
				Swal.fire({
					type: 'warning',
					title: 'Update',
					showConfirmButton: false,
					 html: "Terdapat pembaharuan aplikasi, aplikasi akan dimuat ulang",
					 timer: 1500,
				}).then((result) => {
					if (result.dismiss === Swal.DismissReason.timer) {   
						window.location.reload();
					}
				});
			}
			$rootScope.userData = JSON.parse(userData);
			$rootScope.userData.nama = $rootScope.userData.nama.replace("+", " ");

		}	
	});

}]);	
RdmApp.controller('SidebarController',  function($state, $scope) {
	setTimeout(function(){
		//handleSidebarMenu();
		handleSidebarMenuActiveLink('match', null, $state);
	 }, 1000);
	 $scope.$on('$includeContentLoaded', function() {
		Array.from(document.querySelectorAll(".c-sidebar")).forEach(function (element) {
			Sidebar._sidebarInterface(element);
		  });
    });

});
RdmApp.controller('rdmController', function($rootScope, $state, $scope,$cookies,ApiServer) {
	$rootScope.logResize = function () {
		//setTimeout(function(){
		//	if (devtools.isOpen) {
		//		console.log("Console Open");
				//window.location.href=base_url+"login/dologout";
		//	}else{
		//		console.log("Console Closed");
		//	}
		//}, 1000);
    };
	$rootScope.cekLogin =function(){
		var userData = $cookies.get('rdmData') || "";
		if(userData == ""){
			window.location.reload();
		}
	}
	$rootScope.customepdf = function (doc, leftText, rightText) {
		doc.content.splice(0,1);
		var now = new Date();
		var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
		doc.pageMargins = [20,60,20,40];
		doc.defaultStyle.fontSize = 12;
		doc.styles.tableHeader.fontSize = 14;
		doc['header']=(function() {
			return {
				columns: [
					{
						alignment: 'left',
						italics: true,
						text: leftText,
						fontSize: 14,
						margin: [10,0]
					},
					{
						alignment: 'right',
						fontSize: 14, 
						text: rightText
					}
				],
				margin: 20
			}
		});
		doc['footer']=(function(page, pages) {
			return {
				columns: [
					{
						alignment: 'left',
						text: ['Created on: ', { text: jsDate.toString() }]
					},
					{
						alignment: 'right',
						text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
					}
				],
				margin: 20
			}
		});
		var objLayout = {};
		objLayout['hLineWidth'] = function(i) { return .5; };
		objLayout['vLineWidth'] = function(i) { return .5; };
		objLayout['hLineColor'] = function(i) { return '#aaa'; };
		objLayout['vLineColor'] = function(i) { return '#aaa'; };
		objLayout['paddingLeft'] = function(i) { return 8; };
		objLayout['paddingRight'] = function(i) { return 4; };
		doc.content[0].layout = objLayout;
		return doc;
};
var elapsedTime = 0;
var interval = setInterval(function() {
  timer()
}, 10);
function progressbar(percent) {
  document.getElementById("bar1").style.width = percent + '%';
  document.getElementById("percent1").innerHTML = percent + '%';
}
function timer() {
  if (elapsedTime > 100) {
	if (elapsedTime >= 107) {
		clearInterval(interval);
		$(".pre-loader").hide();
	}
  } else {
	  progressbar(elapsedTime);
  }
  elapsedTime++;
}
});
RdmApp.controller('guruController', function($rootScope, $state, $scope,ApiServer) {
	$rootScope.dataTimeline = [];
	$rootScope.selectKelas = "";
	$rootScope.posts = [];
	$rootScope.datasiswa = [];
	$rootScope.walas = "";
	$rootScope.dataeksekutif = {};
	$rootScope.datakelas = [];
	$rootScope.profile = {};
	$rootScope.getprofile = function() {
		ApiServer.get("guru/profile",function(response) {
			$rootScope.profile = response.data; 
		});
	}
	$rootScope.getprofile();
	$rootScope.getdatakelas = function() {
		ApiServer.get("guru/getkelas",function(response) {
				$rootScope.datakelas = response.data; 
				$rootScope.walas = response.walas; 
				$rootScope.datawalas = response.datawalas; 
				$rootScope.extra = response.extra; 
				$rootScope.dataextra = response.dataextra; 
				$scope.$broadcast("KelasChange",$rootScope.datakelas);
				setTimeout(function(){
					handleSidebarMenuActiveLink('match', null, $state);
				 }, 1000);
		});
	}

	$rootScope.getdatakelas();
	
	//$rootScope.getdataguru();
	//$rootScope.getdataeksekutif();
	//$rootScope.getdatakelas();
	//$rootScope.getdatarombel();
});
RdmApp.controller('proktor', function($rootScope, $state, $scope,ApiServer) {
	$rootScope.datasiswa = {};
	$rootScope.tempdatasiswa = {};
	$rootScope.dataguru = {};
	$rootScope.dataeksekutif = {};
	$rootScope.datakelas = {};
	$rootScope.MapelInduk = {};
	$rootScope.datarombel = {};
	$rootScope.datasemester = {};
	$rootScope.dataekstrakurikuler = {};
	$rootScope.loaddatasiswa = true;

	$rootScope.getdataguru = function() {
		ApiServer.get("proktor/guru/data",function(response) {
				$rootScope.dataguru = response.data; 
		});
	}
	$rootScope.rendering =false;
	$rootScope.chekupdate = function() {
		ApiServer.get("proktor/chekupdate",function(response) {
				if(response.success == false){
					Swal.fire({
						title: 'Update RDM',
						text: "Tersedia update aplikasi!",
						type: 'info',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya, Update sekarang!'
					  }).then((result) => {
						if (result.value) { 
							$rootScope.doupdate("download");
						}
					  })
					
				} 
		});
	}
	$rootScope.updatedone = function() {
		$.blockUI({ 
			message: '<h2>Harap bersabar!!<br>Sedang proses update!</h2>',
			baseZ: 9999
		});
		ApiServer.get("login/updatedone",function(response){
			$.unblockUI({});
			if(response.success){
				Swal.fire({
					type: 'success',
					title: 'Update Berhasil',
					showConfirmButton: true,
					html: "Aplikasi RDM sukses di update ke versi terbaru."
				}).then((result) => {
					if (result.value) { 
						window.location.reload();
					}
				});

			}else{
				Swal.fire({
					type: 'error',
					title: 'Gagal',
					showConfirmButton: true,
					html: "Gagal menyimpan update RDM."
				});
			}
		});
	}
	$rootScope.doupdate = function($string) {
		console.log("Update Confirm"+$string);
		if($string == "download"){
			$.blockUI({ 
				message: '<h2>Harap bersabar!!<br>Sedang mendownload Update!</h2>',
				baseZ: 9999
			});
		}else{
			$.blockUI({ 
				message: '<h2>Harap bersabar!!<br>Sedang proses update!</h2>',
				baseZ: 9999
			});
		}
		ApiServer.get("newupdate.php?"+$string,function(response) {
				$.unblockUI({});
				console.log(response.success);
				if(response.success){
					if($string == "download"){
						$rootScope.doupdate("extract");
					}
					else if($string == "extract"){
						$rootScope.updatedone();
					}
				}else{
					if(response.message !== undefined){
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: response.message
						});
					}
					else if($string == "download"){
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							 html: "Gagal mengunduh file update."
						});
					}
					else if($string == "extract"){
						Swal.fire({
							type: 'error',
							title: 'Gagal',
							showConfirmButton: true,
							html: "Gagal mengextract file update."
						});
					}
				} 
		});
	}
	$rootScope.chekupdate();
	$rootScope.getdatasiswa = function(filter) {
		$.blockUI({});
		ApiServer.post("proktor/siswa/data",filter,function(response) {
			$.unblockUI({});
			$rootScope.datasiswa = response.data;
		});
	}
	$rootScope.getdataajar = function(filter) {
		$.blockUI({});
		ApiServer.post("proktor/ajar/data",filter,function(response) {
			$.unblockUI({});
			$rootScope.dataajar = response.data;
		});
	}
	$rootScope.datasiswa = {};
	$rootScope.pimpinan = {};
	$rootScope.getprofile = function() {
		ApiServer.get("proktor/profile",function(response) {
				$rootScope.profile = response.data; 
				$rootScope.pimpinan = response.pimpinan; 
		});
	}
	$rootScope.getprofile();
	//$rootScope.getdatasiswa(0,200);
	$rootScope.aturcetak = {};
	$rootScope.getdataguru();
	$rootScope.getpengaturan = function() {
		ApiServer.get("proktor/pengaturan/data",function(response) {
			$rootScope.tahunajaran = response.tahunajaran;
			$rootScope.datasemester = response.semester;
			$rootScope.aturcetak = response.aturcetak;
			$rootScope.penilaian.system = response.sistem_penilaian;
			//console.log(response);
		});
	}
	$rootScope.penilaian ={};
	$rootScope.getpengaturan();
	$rootScope.getsemester = function() {
		ApiServer.get("proktor/semester/data",function(response) {
				$rootScope.datasemester = response.data; 
				$rootScope.aturcetak = response.aturcetak; 
				//console.log(response);
		});
	}
	//$rootScope.getsemester();
    $rootScope.gettahunajaran = function() {
		ApiServer.get("proktor/tahunajaran/data",function(response) {
				$rootScope.tahunajaran = response.data; 
		});
	}
	//$rootScope.gettahunajaran();
	$rootScope.dataKelas = {};
    $rootScope.getdatakelas = function() {
		ApiServer.get("proktor/kelas/data",function(response) {
			$rootScope.dataKelas = response.data; 
			$rootScope.getdatatingkat();
		});
	}
	$rootScope.getdatakelas();	
	$rootScope.getdataMapel = function() {
		ApiServer.get("proktor/mapel/data",function(response) {
				$rootScope.dataMapel = response.data; 
				$rootScope.MapelInduk = response.induk; 
		});
	}
	$rootScope.getdataMapel();
	$rootScope.getdatatingkat = function() {
		ApiServer.get("proktor/kelas/datatingkat",function(response) {
				$rootScope.tingkat = response.tingkat; 
				$rootScope.jurusan = response.jurusan; 
				$rootScope.kelas = response.kelas; 
		});
	}
	$rootScope.getdataekstra = function() {
		$rootScope.rendering =false;
		ApiServer.get("proktor/ekstrakurikuler/data",function(response) {
			$.unblockUI({});
			$rootScope.dataekstrakurikuler = response.data; 
			setTimeout(function(){
				$rootScope.rendering =true;
			});
		});
	}
	$rootScope.reloaddataProktor = function() {
		$rootScope.getdataguru();
		$rootScope.getprofile();
		$rootScope.getsemester();	
		$rootScope.gettahunajaran();
		$rootScope.getdataMapel();
		$rootScope.getdatakelas();
	}

	//$rootScope.getdatasiswa();
	//$rootScope.getdataeksekutif();
	//$rootScope.getdatakelas();
	//$rootScope.getdatarombel();
});

